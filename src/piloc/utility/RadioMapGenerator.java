package piloc.utility;
import java.awt.Point;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import piloc.common.Fingerprint;
import piloc.common.PositionIdMap;
import piloc.common.ReadWifiData;


public class RadioMapGenerator {
	
	public static String[] FILE_GEN_RADIO_MAP_TRACES = new String[]{
		"C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 11 Mar 2015/com1 b1/radiomap/com1b1 slow wifi and stop - steptrace 20150311193337.txt",
		"C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 11 Mar 2015/com1 b1/radiomap/com1b1 wifi slow stop partial - steptrace 20150311215049.txt"
	};
	public static String FILE_GEN_RADIO_MAP_POSITION_MAP = "com1b1/posmap.txt";
	public static String FILE_GEN_RADIO_MAP_OUTPUT = "C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 11 Mar 2015/com1 b1/radiomap/radiomap-5.txt";
	
	public static final int LIMIT_WIFI_SCANS_TO_MOST_RECENT_MILLISECONDS = 200;
	public static Integer[] LIMIT_WIFI_SCAN_CHANNELS = {};
	//public static Integer[] LIMIT_WIFI_SCAN_CHANNELS = {2412, 2417, 2422, 2427, 2432, 2437, 2442, 2447, 2452, 2457, 2462, 2467, 2472, 2484};
	//public static Integer[] LIMIT_WIFI_SCAN_CHANNELS = {4915, 4920, 4925, 4935, 4940, 4945, 4960, 4980, 5035, 5040, 5045, 5055, 5060, 5080, 5170, 5180, 5190, 5200, 5210, 5220, 5230, 5240, 5260, 5280, 5300, 5320, 5500, 5520, 5540, 5560, 5580, 5600, 5620, 5640, 5660, 5680, 5700, 5745, 5765, 5785, 5805, 5825};
	public static String[] LIMIT_WIFI_SCAN_SSID = {};
	//public static String[] LIMIT_WIFI_SCAN_SSID = {"NUS"};
	
	public final static boolean INTERPOLATE_BETWEEN_POSITIONS = true;
	
	static PositionIdMap positionIdMap = null;
	static BufferedWriter bw;

	public static void main(String[] args){
		
		positionIdMap = new PositionIdMap(FILE_GEN_RADIO_MAP_POSITION_MAP);
		
		try {
			bw = new BufferedWriter(new FileWriter(FILE_GEN_RADIO_MAP_OUTPUT));
			for (int i = 0; i < FILE_GEN_RADIO_MAP_TRACES.length; i++) {
				generateRadioMapFromSingleTrace(FILE_GEN_RADIO_MAP_TRACES[i]);
			}
			
			bw.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		GetRadioMapStats.main(new String[]{FILE_GEN_RADIO_MAP_OUTPUT});
		
	}

	public static void generateRadioMapFromSingleTrace(String filename) throws FileNotFoundException,
			IOException {
		
		Fingerprint fp = null;
		int positionNumber = 1;
		int prevPositionNumber = -1;
		
		List<Fingerprint> wifiAPsBetweenSteps = new ArrayList<Fingerprint>();
		List<List<Fingerprint>> stepsBetweenPositionIds = new ArrayList<List<Fingerprint>>();
		
		BufferedReader br;
		String line;
		br = new BufferedReader(new FileReader(filename));
		
		while ((line = br.readLine()) != null) {
			String lineParts[] = line.split("\t");
			String timeStamp = lineParts[0];
			String type = lineParts[1];
			
			if (type.equals("WIFI")){
				fp = new Fingerprint();
				ReadWifiData
				.readWifiData(
						br,
						lineParts,
						fp,
						LIMIT_WIFI_SCANS_TO_MOST_RECENT_MILLISECONDS,
						LIMIT_WIFI_SCAN_CHANNELS,
						LIMIT_WIFI_SCAN_SSID);
				wifiAPsBetweenSteps.add(fp);
			}
			
			if (type.equals("POSITIONID")){
				
				prevPositionNumber = positionNumber;
				positionNumber = Integer.parseInt(lineParts[2]);
				
				if (INTERPOLATE_BETWEEN_POSITIONS) {
					Point previousPos = positionIdMap.getLocationOfId(prevPositionNumber);
					Point currPos = positionIdMap.getLocationOfId(positionNumber);
					
					int deltaX = currPos.x - previousPos.x;
					int deltaY = currPos.y - previousPos.y;
					
					double deltaStepX = (double) deltaX / stepsBetweenPositionIds.size();
					double deltaStepY = (double) deltaY / stepsBetweenPositionIds.size();
					
					for (int i=0; i<stepsBetweenPositionIds.size(); i++) {
						List<Fingerprint> fingerprintsAtStep = stepsBetweenPositionIds.get(i);
						for (int j=0; j<fingerprintsAtStep.size(); j++) {
							if (fingerprintsAtStep.get(j).getBssids().size() > 0) {
								bw.write(String.format("%d %d", (int) (previousPos.x + deltaStepX*i), (int) (previousPos.y + deltaStepY*i)));
								Set<String> bssids = fingerprintsAtStep.get(j).getBssids();
								for(String bssid : bssids) {
									bw.write(String.format(" %s %d", bssid, fingerprintsAtStep.get(j).getRssi(bssid)));
								}
								bw.newLine();
							}
						}													
					}
					
					stepsBetweenPositionIds.clear();
				}
				
				if (fp != null) {
					Point pos = positionIdMap.getLocationOfId(positionNumber);
					bw.write(String.format("%d %d", pos.x, pos.y));
					
					Set<String> bssids = fp.getBssids();
					for(String bssid : bssids) {
						bw.write(String.format(" %s %d", bssid, fp.getRssi(bssid)));
					}
					bw.newLine();
				}
				
			}
			
			if (type.equals("STEP")){
				stepsBetweenPositionIds.add(wifiAPsBetweenSteps);
				wifiAPsBetweenSteps = new ArrayList<Fingerprint>();					
			}
			
		}
		br.close();
	}
	
	/*private void findShortestPath(BufferedImage mapImage, Point start, Point end) {
		new Color(mapImage.getRGB((int) p.getX(), (int) p.getY())).equals(Color.MAGENTA)
	}*/

}
