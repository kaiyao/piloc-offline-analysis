package piloc.utility;

public class BatchRadioMapAnalysis {
	
	/*
	public static String[] FILE_GEN_RADIO_MAP_TRACES = new String[]{
		"C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 11 Mar 2015/com1 b1/radiomap/com1b1 slow wifi and stop - steptrace 20150311193337.txt",
		"C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 11 Mar 2015/com1 b1/radiomap/com1b1 wifi slow stop partial - steptrace 20150311215049.txt"
	};
	public static String FILE_GEN_RADIO_MAP_POSITION_MAP = "com1b1/posmap.txt";
	public static String FILE_GEN_RADIO_MAP_OUTPUT = "C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 11 Mar 2015/com1 b1/radiomap/radiomap-%s.txt";
	*/
	
	/*
	public static String[] FILE_GEN_RADIO_MAP_TRACES = new String[]{
		"C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 18 Mar 2015/com1l1/radiomap/steptrace 20150318192044.txt",
		"C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 18 Mar 2015/com1l1/radiomap/steptrace 20150318193337.txt"
	};
	public static String FILE_GEN_RADIO_MAP_POSITION_MAP = "com1l1/posmap.txt";
	public static String FILE_GEN_RADIO_MAP_OUTPUT = "C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 18 Mar 2015/com1l1/radiomap/radiomap-%s.txt";
	*/
	/*
	public static String[] FILE_GEN_RADIO_MAP_TRACES = new String[]{
		"C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 20 Mar 2015/createl2/radiomap/steptrace 20150320211822.txt",
		"C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 20 Mar 2015/createl2/radiomap/steptrace 20150320213245.txt",
		"C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 20 Mar 2015/createl2/radiomap/steptrace 20150320212611.txt"
	};
	public static String FILE_GEN_RADIO_MAP_POSITION_MAP = "createl2/posmap.txt";
	public static String FILE_GEN_RADIO_MAP_OUTPUT = "C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 20 Mar 2015/createl2/radiomap/radiomap-%s.txt";
	*/
	public static String[] FILE_GEN_RADIO_MAP_TRACES = new String[]{
		"C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 11 Mar 2015/i3 lvl3/radiomap/i3 lvl3 anticlockwise wifi slow stop - steptrace 20150311201754.txt", 
		"C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 11 Mar 2015/i3 lvl3/radiomap/i3 lvl3 clockwise wifi slow mostly stop - steptrace 20150311210829.txt", 
		"C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 11 Mar 2015/i3 lvl3/radiomap/i3 lvl3 clockwise wifi slow stop - steptrace 20150311203149.txt"
	};
	public static String FILE_GEN_RADIO_MAP_POSITION_MAP = "i3/posmap.txt";
	public static String FILE_GEN_RADIO_MAP_OUTPUT = "C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 11 Mar 2015/i3 lvl3/radiomap/radiomap-%s.txt";
	
	public static Integer[] LIMIT_WIFI_SCAN_CHANNELS = {};
	public static final Integer[] CHANNELS2 = {2412, 2417, 2422, 2427, 2432, 2437, 2442, 2447, 2452, 2457, 2462, 2467, 2472, 2484};
	public static final Integer[] CHANNELS5 = {4915, 4920, 4925, 4935, 4940, 4945, 4960, 4980, 5035, 5040, 5045, 5055, 5060, 5080, 5170, 5180, 5190, 5200, 5210, 5220, 5230, 5240, 5260, 5280, 5300, 5320, 5500, 5520, 5540, 5560, 5580, 5600, 5620, 5640, 5660, 5680, 5700, 5745, 5765, 5785, 5805, 5825};
	public static String[] LIMIT_WIFI_SCAN_SSID = {};
	//public static String[] LIMIT_WIFI_SCAN_SSID = {"NUS"};

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		RadioMapGenerator.FILE_GEN_RADIO_MAP_TRACES = FILE_GEN_RADIO_MAP_TRACES;
		RadioMapGenerator.FILE_GEN_RADIO_MAP_POSITION_MAP = FILE_GEN_RADIO_MAP_POSITION_MAP;
				
		System.out.println("All Wifi");
		RadioMapGenerator.LIMIT_WIFI_SCAN_CHANNELS = new Integer[]{};		
		RadioMapGenerator.LIMIT_WIFI_SCAN_SSID = new String[]{};
		RadioMapGenerator.FILE_GEN_RADIO_MAP_OUTPUT = String.format(FILE_GEN_RADIO_MAP_OUTPUT, "all");
		RadioMapGenerator.main(new String[]{});
		
		System.out.println("All NUS");
		RadioMapGenerator.LIMIT_WIFI_SCAN_CHANNELS = new Integer[]{};		
		RadioMapGenerator.LIMIT_WIFI_SCAN_SSID = new String[]{"NUS"};
		RadioMapGenerator.FILE_GEN_RADIO_MAP_OUTPUT = String.format(FILE_GEN_RADIO_MAP_OUTPUT, "NUS");
		RadioMapGenerator.main(new String[]{});
		
		System.out.println("5GHz");
		RadioMapGenerator.LIMIT_WIFI_SCAN_CHANNELS = CHANNELS5;		
		RadioMapGenerator.LIMIT_WIFI_SCAN_SSID = new String[]{};
		RadioMapGenerator.FILE_GEN_RADIO_MAP_OUTPUT = String.format(FILE_GEN_RADIO_MAP_OUTPUT, "5");
		RadioMapGenerator.main(new String[]{});
		
		System.out.println("5GHz + NUS");
		RadioMapGenerator.LIMIT_WIFI_SCAN_CHANNELS = CHANNELS5;		
		RadioMapGenerator.LIMIT_WIFI_SCAN_SSID = new String[]{"NUS"};
		RadioMapGenerator.FILE_GEN_RADIO_MAP_OUTPUT = String.format(FILE_GEN_RADIO_MAP_OUTPUT, "5+NUS");
		RadioMapGenerator.main(new String[]{});
		
		System.out.println("2.4GHz");
		RadioMapGenerator.LIMIT_WIFI_SCAN_CHANNELS = CHANNELS2;		
		RadioMapGenerator.LIMIT_WIFI_SCAN_SSID = new String[]{};
		RadioMapGenerator.FILE_GEN_RADIO_MAP_OUTPUT = String.format(FILE_GEN_RADIO_MAP_OUTPUT, "2.4");
		RadioMapGenerator.main(new String[]{});
		
		System.out.println("2.4GHz + NUS");
		RadioMapGenerator.LIMIT_WIFI_SCAN_CHANNELS = CHANNELS2;		
		RadioMapGenerator.LIMIT_WIFI_SCAN_SSID = new String[]{"NUS"};
		RadioMapGenerator.FILE_GEN_RADIO_MAP_OUTPUT = String.format(FILE_GEN_RADIO_MAP_OUTPUT, "2.4+NUS");
		RadioMapGenerator.main(new String[]{});

	}

}
