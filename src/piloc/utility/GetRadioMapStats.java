package piloc.utility;

import piloc.common.RadioMap;

public class GetRadioMapStats {

	public static void main(String[] args) {

		String filename;
		if (args.length < 1) {
			filename = "C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 11 Mar 2015/i3 lvl3/radiomap/radiomap.txt";
		}else{
			filename = args[0];
		}
		
		
		RadioMap radioMap = new RadioMap(filename);
		
		System.out.println(radioMap.getAccessPointSet());
		System.out.println("AP Count: " + radioMap.getAccessPointSet().size());
		System.out.println("AP Point Min: " + radioMap.getMinPointApCount());
		System.out.println("AP Point Max: " + radioMap.getMaxPointApCount());
	}

}
