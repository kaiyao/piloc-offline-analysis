package piloc.utility;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import piloc.common.PositionIdMap;

public class PositionIdVisualizer {

	public static void main(String[] args) throws IOException {
		
		BufferedImage backgroundImg = ImageIO.read(new File("i3/icubel3background.png"));
		PositionIdMap posMap = new PositionIdMap("i3/posmap.txt");
		
		try {
			BufferedImage img = backgroundImg;
			Graphics2D graphic = img.createGraphics();
			
			graphic.setFont(new Font("Tahoma", Font.PLAIN, 11)); 
			
			for(int i=1; i<posMap.getNumberOfPositions(); i++){
				Point point = posMap.getLocationOfId(i);
				graphic.setColor(Color.black);
				graphic.drawString(String.format("%d", i), (int) point.getX(), (int) point.getY());
			}

			ImageIO.write(img, "PNG", new File("positionIdVisualizationCOM1L1.png"));
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
