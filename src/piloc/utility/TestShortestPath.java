package piloc.utility;

import java.awt.Color;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import javax.imageio.ImageIO;

import piloc.common.utility.PixelMatchCriteria;
import piloc.common.utility.ShortestPathFinder;
import piloc.common.utility.ShortestPathFinder.SPath;

public class TestShortestPath {

	Point[][] discoveredPoints;
	Queue<Point> queue;
	BufferedImage img;
	
	List<Point> validPixels;
	
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		new TestShortestPath().doStuff();
	}
	
	public void doStuff() throws IOException{
		img = ImageIO.read(new File("com1l1/com1l1path.png"));
		//getShortestPath();
		// System.out.println(getNearestValidPixel(new Point(982, 13)));
		
		PixelMatchCriteria pmc = new PixelMatchCriteria(){

			@Override
			public boolean checkPixelAtPositionMatchesCriteria(
					BufferedImage img, int x, int y) {
				return new Color(img.getRGB(x, y)).equals(Color.BLACK);
			}
			
		};
		
		Point startPoint = new Point(545,73);
		Point endPoint = new Point(664,246);
		
		SPath a = new ShortestPathFinder(img, pmc).getShortestPath(startPoint, endPoint);
		System.out.println(a.getPath());
		System.out.println(a.getPathLength());
	}
	
	public void getShortestPath() throws IOException {
		
		discoveredPoints = new Point[img.getWidth()][img.getHeight()];
		queue = new LinkedList<Point>();
		
		Point startPoint = new Point(545,73);
		Point endPoint = new Point(664,246);
		boolean isPathFound = false;
		
		queue.offer(startPoint);
		
		while(!queue.isEmpty()){
			Point p = queue.poll();
			System.out.println(p);
			if (p.equals(endPoint)) {
				isPathFound = true;
				break;
			}

			int x = p.x;
			int y = p.y;
			
			if (checkPixelMatchesCriteria(x, y)){				
				
				addPointToQueueIfNotYetVisited(x - 1, y - 1, x, y); // Top left
				addPointToQueueIfNotYetVisited(x    , y - 1, x, y); // Top center
				addPointToQueueIfNotYetVisited(x + 1, y - 1, x, y); // Top right
				
				addPointToQueueIfNotYetVisited(x - 1, y    , x, y); // Left
				addPointToQueueIfNotYetVisited(x + 1, y    , x, y); // Right
				
				addPointToQueueIfNotYetVisited(x - 1, y + 1, x, y); // Bottom left
				addPointToQueueIfNotYetVisited(x    , y + 1, x, y); // Bottom center
				addPointToQueueIfNotYetVisited(x + 1, y + 1, x, y); // Bottom right
				
			}
		}
		
		if (isPathFound) {
			Point currPoint = endPoint;
			while(!currPoint.equals(startPoint)) {
				img.setRGB(currPoint.x, currPoint.y, Color.GREEN.getRGB());
				currPoint = discoveredPoints[currPoint.x][currPoint.y];
			}
			
			ImageIO.write(img, "PNG", new File("shortestpath.png"));
		}
	}
	
	public Point getNearestValidPixel(Point givenPoint) {
		
		// http://stackoverflow.com/questions/307445/finding-closest-non-black-pixel-in-an-image-fast
		
		if (checkPixelMatchesCriteria(givenPoint.x, givenPoint.y)){
			return givenPoint;
		}
		
		validPixels = new LinkedList<Point>();
		
		int squareSearchDistance = 1;
		while (validPixels.isEmpty()) {
			searchSquare(squareSearchDistance, givenPoint);
			squareSearchDistance++;
		}
		
		System.out.println(validPixels);
		
		int maxDistanceToSearch = squareSearchDistance + (int) Math.ceil(validPixels.get(0).distance(givenPoint));
		while (squareSearchDistance < maxDistanceToSearch) {
			searchSquare(squareSearchDistance, givenPoint);
			squareSearchDistance++;
		}
		
		System.out.println(validPixels);
		
		Point nearestPoint = validPixels.get(0);
		double nearestDistanceSq = nearestPoint.distanceSq(givenPoint);
		
		for(Point p : validPixels) {
			double distanceSq = p.distanceSq(givenPoint);
			if (distanceSq < nearestDistanceSq) {
				nearestDistanceSq = distanceSq;
				nearestPoint = p;
			}
		}
		
		return nearestPoint;
	}

	public void searchSquare(int squareSearchDistance, Point aroundPoint) {
		
		Point p;
		
		// Search top and bottom rows
		int leftBound = aroundPoint.x - squareSearchDistance;
		if (leftBound < 0) leftBound = 0;
		
		int rightBound = aroundPoint.x + squareSearchDistance;
		if (rightBound >= img.getWidth()) rightBound = img.getWidth() - 1;
		
		int topRowY = aroundPoint.y - squareSearchDistance;
		if (topRowY < 0) topRowY = 0;
		
		int bottomRowY = aroundPoint.y + squareSearchDistance;
		if (bottomRowY >= img.getHeight()) bottomRowY = img.getHeight() - 1;
		
		for (int x = leftBound; x <= rightBound; x++) {
			
			// Check top row pixels
			p = new Point(x, topRowY);
			//System.out.println("Top " + p);
			if (checkPixelMatchesCriteria(p.x, p.y)){
				validPixels.add(p);
			}
			
			// Check bottom row pixels
			p = new Point(x, bottomRowY);
			// System.out.println("Bot " + p);
			if (checkPixelMatchesCriteria(p.x, p.y)) {
				validPixels.add(p);
			}
			
		}
		
		// Search left and right columns
		int topBound = aroundPoint.y - squareSearchDistance + 1;
		if (topBound < 0) topBound = 0;
		int bottomBound = aroundPoint.y + squareSearchDistance - 1;
		if (bottomBound >= img.getHeight()) bottomBound = img.getHeight() - 1;
		
		int leftCol = aroundPoint.x - squareSearchDistance;
		if (leftCol < 0) leftCol = 0;
		
		int rightCol = aroundPoint.x + squareSearchDistance;
		if (rightCol >= img.getWidth()) rightCol = img.getWidth() - 1;
		
		for (int y = topBound; y <= bottomBound; y++) {

			// Check left column pixels
			p = new Point(leftCol, y);
			//System.out.println("Lef Row" + p);
			if (checkPixelMatchesCriteria(p.x, p.y)) {
				validPixels.add(p);
			}

			// Check right column pixels
			p = new Point(rightCol, y);
			// System.out.println("Rig Row" + p);
			if (checkPixelMatchesCriteria(p.x, p.y)) {
				validPixels.add(p);
			}

		}
	}
	
	/**
	 * This method doesn't work as well as searching every possible point on route.
	 * This is because it searches a square, which means that it may pick the 
	 * corner of the square instead of the absolute closest pixel
	 * @param givenPoint
	 * @return
	 * @throws IOException 
	 */
	public Point getNearestValidPixelNotAccurate(Point givenPoint) throws IOException {
		boolean visitedPoints[][] = new boolean[img.getWidth()][img.getHeight()];
		
		BufferedImage img2 = copyImage(img);
		
		Queue<Point> qu = new LinkedList<Point>();
		qu.offer(new Point(givenPoint.x, givenPoint.y));
		
		Point foundPoint = null;
				
		while (!qu.isEmpty()) {
			Point p = qu.poll();
			if (p.x < 0 || p.y < 0 || p.x >= img.getWidth()
					|| p.y >= img.getHeight() || visitedPoints[p.x][p.y]) {
				continue;
			}
			visitedPoints[p.x][p.y] = true;
			img2.setRGB(p.x, p.y, Color.GREEN.getRGB());
			System.out.println(p);
			
			int x = p.x;
			int y = p.y;
			
			if (checkPixelMatchesCriteria(x, y)){
				foundPoint = new Point(x, y);
				break;
			}
				
			qu.offer(new Point(x - 1, y - 1)); // Top left
			qu.offer(new Point(x, y - 1)); // Top center
			qu.offer(new Point(x + 1, y - 1)); // Top right

			qu.offer(new Point(x - 1, y)); // Left
			qu.offer(new Point(x + 1, y)); // Right

			qu.offer(new Point(x - 1, y + 1)); // Bottom left
			qu.offer(new Point(x, y + 1)); // Bottom center
			qu.offer(new Point(x + 1, y + 1)); // Bottom right
				
		}
		
		ImageIO.write(img2, "PNG", new File("nearestpoint.png"));
		
		return foundPoint;
	}

	public boolean checkPixelMatchesCriteria(int x, int y) {
		return new Color(img.getRGB(x, y)).equals(Color.BLACK);
	}

	public void addPointToQueueIfNotYetVisited(int x, int y, int predX, int predY) {
		if (!(discoveredPoints[x][y] instanceof Point)) {
			discoveredPoints[x][y] = new Point(predX, predY);
			queue.offer(new Point(x, y));
		}
	}
	
	static BufferedImage copyImage(BufferedImage bi) {
		ColorModel cm = bi.getColorModel();
		boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
		WritableRaster raster = bi.copyData(null);
		return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
		//return new BufferedImage(bi.getWidth(), bi.getHeight(), BufferedImage.TYPE_INT_ARGB);
	}

}
