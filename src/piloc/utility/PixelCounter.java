package piloc.utility;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import piloc.common.utility.PixelMatchCriteria;

public class PixelCounter {

	public static void main(String[] args) throws IOException {
		
		
		PixelMatchCriteria pmc = new PixelMatchCriteria(){

			@Override
			public boolean checkPixelAtPositionMatchesCriteria(
					BufferedImage img, int x, int y) {
				boolean isMagenta = new Color(img.getRGB(x, y)).equals(Color.MAGENTA);
				return !isMagenta;
			}
			
		};
		
		System.out.println("COM1 L1");
		BufferedImage img = ImageIO.read(new File("com1b1/com1b1mask.png"));
		int pixelCount = countImgPixels(img, pmc);		
		System.out.println(pixelCount);
		
		System.out.println("COM1 L1");
		img = ImageIO.read(new File("com1l1/com1l1mask.png"));
		pixelCount = countImgPixels(img, pmc);		
		System.out.println(pixelCount);
		
		System.out.println("CREATE L2");
		img = ImageIO.read(new File("createl2/mask.png"));
		pixelCount = countImgPixels(img, pmc);		
		System.out.println(pixelCount);
		
		System.out.println("I3 L3");
		img = ImageIO.read(new File("i3/icubel3mask.png"));
		pixelCount = countImgPixels(img, pmc);
		System.out.println(pixelCount);

	}

	public static int countImgPixels(BufferedImage img, PixelMatchCriteria pmc) {
		int pixelCount = 0;
		for (int x = 0; x < img.getWidth(); x++) {
			for (int y = 0; y < img.getHeight(); y++) {
				if (pmc.checkPixelAtPositionMatchesCriteria(img, x, y)) {
					pixelCount++;
				}
			}
		}
		return pixelCount;
	}

}
