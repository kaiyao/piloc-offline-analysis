package piloc.analysis;


import java.awt.Point;
import java.util.List;
import java.util.Random;

import org.apache.commons.math3.distribution.NormalDistribution;

import piloc.common.FingerprintScore;

public class Particle {
	
	private float x;
	private float y;
	private float headingOffset;
	private float stepLength = Float.NEGATIVE_INFINITY;
	
	public Particle(float x, float y) {
		super();
		this.x = x;
		this.y = y;
	}
	
	public Particle (Particle p){
		this.x = p.x;
		this.y = p.y;
		this.stepLength = p.stepLength;
		this.headingOffset = p.headingOffset;
	}
	
	public Particle(float x, float y, Particle referenceParticle) {
		super();
		this.x = x;
		this.y = y;
		this.stepLength = referenceParticle.stepLength;
		this.headingOffset = referenceParticle.headingOffset;
	}

	public float getX() {
		return x;
	}
	
	public float getY() {
		return y;
	}
	
	public float getHeadingOffset() {
		return headingOffset;
	}

	public float getStepLength() {
		return stepLength;
	}

	public void randomize(float maxX, float maxY) {
		this.x = (float) (Math.random() * maxX);
		this.y = (float) (Math.random() * maxY);
	}
	
	public void moveStep(float bearing){
		
		// Model the noise
		float distance;
		
		if (Params.ENABLE_STEP_LENGTH_ESTIMATION){
			if (stepLength == Float.NEGATIVE_INFINITY) { // Initialize
				stepLength = uniformRandomInRange(Params.getMotionStepMin(), Params.getMotionStepMax());
			}
			NormalDistribution nd = new NormalDistribution(stepLength, Params.MOTION_STEP_LENGTH_SD);
			distance = (float) nd.sample();
			stepLength = (float) (Params.MOTION_STEP_LENGTH_WEIGHT * distance + (1-Params.MOTION_STEP_LENGTH_WEIGHT) * stepLength);
		}else{
			distance = uniformRandomInRange(Params.getMotionStepMin(), Params.getMotionStepMax());
		}
		//bearing += uniformRandomInRange(bearing - 30, bearing + 30); // +/- 30 deg
		
		double bearingInRadians;
		
		if (Params.ENABLE_COMPASS) {
			NormalDistribution nd = new NormalDistribution(bearing, Params.MOTION_ANGLE_SD); // normal dist sd = 15
			bearing = (float) nd.sample();
			
			bearingInRadians = bearing * Math.PI / 180.0;
		}else{
			bearingInRadians = uniformRandomInRange(0, (float) (2*Math.PI));
		}
		
		float newX = (float) (x + distance * Math.sin(bearingInRadians));
		float newY = (float) (y - distance * Math.cos(bearingInRadians));
		
		//Log.v("", String.format("BeforeMove %f %f AfterMove %f %f", x, y, newX, newY));
		
		this.x = newX;
		this.y = newY;
	}
	
	public double senseWithMeasurement(List<FingerprintScore> matches){
		
		/*
		double weight = 0;
		
		double sd = Params.getSenseMeasurementSd();
		NormalDistribution nd = new NormalDistribution(0, sd);
		
		int pointCount = 0;
		
		for (Point p : points) {
			double distanceToP = Math.sqrt(Math.pow(p.x - x, 2) + Math.pow(p.y - y, 2));
			
			float pointWeight = Params.POINTS_PROPORTION[Params.POINTS_PROPORTION.length - 1];
			if (pointCount < Params.POINTS_PROPORTION.length){
				pointWeight = Params.POINTS_PROPORTION[pointCount];
			}
			weight += nd.density(distanceToP) * pointWeight;
			pointCount++;
		}
		*/
		
		
		// The weight is given by the nearest fingerprint's score
		switch (Params.WEIGHT_METHOD) {
		
		case NEAREST_NEIGHBOUR:
			
			double minDistance = Double.MAX_VALUE;
			FingerprintScore nearestScore = null;
			
			for (FingerprintScore score : matches) {
				
				double distanceBetweenFingerprintAndPoint = 
						Math.sqrt(
								Math.pow(score.getFingerprint().getX() - x, 2) +
								Math.pow(score.getFingerprint().getY() - y, 2)
								);
				
				if (distanceBetweenFingerprintAndPoint < minDistance) {
					minDistance = distanceBetweenFingerprintAndPoint;
					nearestScore = score;
				}
			}
			
			return nearestScore.getScore();
			
		case HIGHEST_IN_VICINITY:
		
			// The weight is given by the highest score in the vicinity
			FingerprintScore highestScore = null;
			for (FingerprintScore score : matches) {
				
				double distanceBetweenFingerprintAndPoint = 
						Math.sqrt(
								Math.pow(score.getFingerprint().getX() - x, 2) +
								Math.pow(score.getFingerprint().getY() - y, 2)
								);
				
				if (distanceBetweenFingerprintAndPoint <= Params.getVicinityRange()) {
					if (highestScore == null || score.getScore() > highestScore.getScore()){
						highestScore = score;
					}
				}
			}
			
			if (highestScore == null){
				// Particle is so off that no Wifi Fingerprints nearby?
				return 0;
			}else{
				return highestScore.getScore();
			}
		default:
			return 0;
			
		}
	}
	
	private float uniformRandomInRange(float min, float max) {
		float rangeSize = max - min;
		return (float) (Math.random() * rangeSize + min);
	}
	

}
