package piloc.analysis;

/* ===========================================================
 * JFreeChart : a free chart library for the Java(tm) platform
 * ===========================================================
 *
 * (C) Copyright 2000-2004, by Object Refinery Limited and Contributors.
 *
 * Project Info:  http://www.jfree.org/jfreechart/index.html
 *
 * This library is free software; you can redistribute it and/or modify it under the terms
 * of the GNU Lesser General Public License as published by the Free Software Foundation;
 * either version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License along with this
 * library; if not, write to the Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 *
 * [Java is a trademark or registered trademark of Sun Microsystems, Inc. 
 * in the United States and other countries.]
 *
 * -----------------
 * DualAxisDemo.java
 * -----------------
 * (C) Copyright 2002-2004, by Object Refinery Limited and Contributors.
 *
 * Original Author:  David Gilbert (for Object Refinery Limited);
 * Contributor(s):   -;
 *
 * $Id: DualAxisDemo.java,v 1.21 2004/05/10 16:45:24 mungady Exp $
 *
 * Changes
 * -------
 * 19-Nov-2002 : Version 1 (DG);
 *
 */

import java.awt.Color;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.AxisLocation;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.CategoryLabelPositions;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.labels.StandardCategoryToolTipGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.DatasetRenderingOrder;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.renderer.category.LineAndShapeRenderer;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.ui.ApplicationFrame;
import org.jfree.ui.RefineryUtilities;

// http://www.java2s.com/Code/Java/Chart/JFreeChartDualAxisDemo.htm
// http://stackoverflow.com/questions/5272966/jfreechart-image
// http://stackoverflow.com/questions/8557363/how-to-set-plot-rendering-order-in-jfree-chart

/**
 * A simple demonstration application showing how to create a dual axis chart
 * based on data from two {@link CategoryDataset} instances.
 *
 */
public class GraphPlotter {
	
	private int wifiHistogramBins[];
	private int algoHistogramBins[];
	
	public GraphPlotter(final String title, final int[] wifiHistogramBins2, final int[] algoHistogramBins2, final String saveImagePath) throws IOException {
		this.wifiHistogramBins = wifiHistogramBins2;
		this.algoHistogramBins = algoHistogramBins2;

		final CategoryDataset dataset1 = createDataset1();

		// create the chart...
		final JFreeChart chart = ChartFactory.createBarChart(title, // chart
																	// title
				"Error(m)", // domain axis label
				"Frequency", // range axis label
				dataset1, // data
				PlotOrientation.VERTICAL, true, // include legend
				true, // tooltips?
				false // URL generator? Not required...
				);

		// NOW DO SOME OPTIONAL CUSTOMISATION OF THE CHART...
		chart.setBackgroundPaint(Color.white);
		// chart.getLegend().setAnchor(Legend.SOUTH);

		// get a reference to the plot for further customisation...
		final CategoryPlot plot = chart.getCategoryPlot();
		plot.setBackgroundPaint(new Color(0xEE, 0xEE, 0xFF));
		plot.setDomainAxisLocation(AxisLocation.BOTTOM_OR_RIGHT);
		plot.getRenderer().setSeriesPaint(0, new Color(96, 74, 123));
		plot.getRenderer().setSeriesPaint(1, new Color(119, 147, 60));

		final CategoryDataset dataset2 = createDataset2();
		plot.setDataset(1, dataset2);
		plot.mapDatasetToRangeAxis(1, 1);

		final CategoryAxis domainAxis = plot.getDomainAxis();
		domainAxis.setCategoryLabelPositions(CategoryLabelPositions.DOWN_45);
		final ValueAxis axis2 = new NumberAxis("Cumulative");
		plot.setRangeAxis(1, axis2);

		final LineAndShapeRenderer renderer2 = new LineAndShapeRenderer();
		renderer2.setSeriesPaint(0, Color.red);
		renderer2.setSeriesPaint(1, Color.blue);
		plot.setRenderer(1, renderer2);
		plot.setDatasetRenderingOrder(DatasetRenderingOrder.FORWARD);
		// OPTIONAL CUSTOMISATION COMPLETED.

		// add the chart to a panel...
		//final ChartPanel chartPanel = new ChartPanel(chart);
		//chartPanel.setPreferredSize(new java.awt.Dimension(500, 270));
		
		FileOutputStream fos = new FileOutputStream(saveImagePath);
		ChartUtilities.writeChartAsPNG(fos,chart,800,600);
		fos.close();

	}

	// ****************************************************************************
	// * JFREECHART DEVELOPER GUIDE *
	// * The JFreeChart Developer Guide, written by David Gilbert, is available
	// *
	// * to purchase from Object Refinery Limited: *
	// * *
	// * http://www.object-refinery.com/jfreechart/guide.html *
	// * *
	// * Sales are used to provide funding for the JFreeChart project - please *
	// * support us so that we can continue developing free software. *
	// ****************************************************************************

	/**
	 * Creates a sample dataset.
	 *
	 * @return The dataset.
	 */
	private CategoryDataset createDataset1() {

		// row keys...
		final String series1 = "WiFi Only";
		final String series2 = "Particle Filter";

		/*
		// column keys...
		final String category1 = "1";
		final String category2 = "2";
		final String category3 = "3";
		final String category4 = "4";
		final String category5 = "5";
		final String category6 = "6";
		final String category7 = "7";
		final String category8 = "8";

		// create the dataset...
		final DefaultCategoryDataset dataset = new DefaultCategoryDataset();

		dataset.addValue(1.0, series1, category1);
		dataset.addValue(4.0, series1, category2);
		dataset.addValue(3.0, series1, category3);
		dataset.addValue(5.0, series1, category4);
		dataset.addValue(5.0, series1, category5);
		dataset.addValue(7.0, series1, category6);
		dataset.addValue(7.0, series1, category7);
		dataset.addValue(8.0, series1, category8);
		
		dataset.addValue(1.0, series2, category1);
		dataset.addValue(4.0, series2, category2);
		dataset.addValue(3.0, series2, category3);
		dataset.addValue(5.0, series2, category4);
		dataset.addValue(5.0, series2, category5);
		dataset.addValue(7.0, series2, category6);
		dataset.addValue(7.0, series2, category7);
		dataset.addValue(8.0, series2, category8);*/
		
		final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		
		for(int i=0; i < wifiHistogramBins.length; i++){
			String category = (i+1)+"";
			if (i >= 10){
				category = "11+";
			}
			dataset.addValue(wifiHistogramBins[i], series1, category);
		}
		
		for(int i=0; i < algoHistogramBins.length; i++){
			String category = (i+1)+"";
			if (i >= 10){
				category = "11+";
			}
			dataset.addValue(algoHistogramBins[i], series2, category);
		}

		return dataset;

	}

	/**
	 * Creates a sample dataset.
	 *
	 * @return The dataset.
	 */
	private CategoryDataset createDataset2() {

		// row keys...
		final String series1 = "WiFi Only";
		final String series2 = "Particle Filter";

		/*
		// column keys...
		final String category1 = "1";
		final String category2 = "2";
		final String category3 = "3";
		final String category4 = "4";
		final String category5 = "5";
		final String category6 = "6";
		final String category7 = "7";
		final String category8 = "8";

		// create the dataset...
		final DefaultCategoryDataset dataset = new DefaultCategoryDataset();

		dataset.addValue(15.0, series1, category1);
		dataset.addValue(24.0, series1, category2);
		dataset.addValue(31.0, series1, category3);
		dataset.addValue(25.0, series1, category4);
		dataset.addValue(56.0, series1, category5);
		dataset.addValue(37.0, series1, category6);
		dataset.addValue(77.0, series1, category7);
		dataset.addValue(18.0, series1, category8);
		*/
		
		final DefaultCategoryDataset dataset = new DefaultCategoryDataset();
		
		double totalWifi = 0;		
		for(int i=0; i < wifiHistogramBins.length; i++){
			totalWifi+=wifiHistogramBins[i];
		}
		
		double cumulateWifi = 0;
		for(int i=0; i < wifiHistogramBins.length; i++){
			cumulateWifi+=wifiHistogramBins[i];
			String category = (i+1)+"";
			if (i >= 10){
				category = "11+";
			}
			dataset.addValue(cumulateWifi/totalWifi, series1, category);
		}
		
		double total = 0;		
		for(int i=0; i < algoHistogramBins.length; i++){
			total+=algoHistogramBins[i];
		}
		
		double cumulate = 0;
		for(int i=0; i < algoHistogramBins.length; i++){
			cumulate+=algoHistogramBins[i];
			String category = (i+1)+"";
			if (i >= 10){
				category = "11+";
			}
			dataset.addValue(cumulate/total, series2, category);
		}

		return dataset;

	}

}
