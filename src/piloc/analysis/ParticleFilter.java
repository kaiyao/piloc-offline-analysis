package piloc.analysis;
import java.awt.Color;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Random;
import java.util.Vector;

import javax.imageio.ImageIO;

import piloc.common.FingerprintScore;

public class ParticleFilter {
	
	private int particleNum = Params.NUM_PARTICLES;
	private List<Particle> particles;
	
	public boolean isFirstSense = true;
	
	private int roomWidth;
	private int roomHeight;
	
	public ParticleFilter(int roomWidth, int roomHeight) {
		super();
		this.roomWidth = roomWidth;
		this.roomHeight = roomHeight;
		
		particles = new Vector<Particle>();
		for (int i = 0; i < particleNum; i++) {
			Particle p = new Particle(0,0);
			p.randomize(roomWidth, roomHeight);
			particles.add(p);
		}
	}

	public synchronized void initializeParticles(List<FingerprintScore> matches){
		
		// We adjust the score such that the closer matches have a higher value
		
		this.particles = createParticlesWithDistributionOfFingerprints(matches, particleNum);
	}

	private Vector<Particle> createParticlesWithDistributionOfFingerprints(List<FingerprintScore> matches, int numParticlesToGenerate) {
		
		// Generate distribution in area (by max score in sample)
		int xDim = (int) (roomWidth/Params.getWifiWeightSampleRange());
		int yDim = (int) (roomHeight/Params.getWifiWeightSampleRange());
		double scoreDistribution[][] = new double[xDim][yDim];
		
		// Fill up distribution array with the maximum score found in that cell
		for (FingerprintScore match : matches) {
			int x = (int) (match.getFingerprint().getX() / Params.getWifiWeightSampleRange());
			int y = (int) (match.getFingerprint().getY() / Params.getWifiWeightSampleRange());
			if (match.getScore() > scoreDistribution[x][y]) {
				scoreDistribution[x][y] = match.getScore();
			}
		}
		
		// Calculate maximum score and total score among all cells
		double sumScore = 0;
		double maxScore = 0;
		for (int x = 0; x < xDim; x++) {
			for (int y = 0; y < yDim; y++) {
				double score = scoreDistribution[x][y];
				sumScore += score;
				if (score > maxScore) {
					maxScore = score;
				}
			}
		}
		
		// Create particles
		Vector<Particle> particles = new Vector<Particle>();
		int pointNumber = 0;
		
		for (int x = 0; x < xDim; x++) {
			for (int y = 0; y < yDim; y++) {
				double score = scoreDistribution[x][y];
			
				int xCoord = (int) ((x + 0.5) * Params.getWifiWeightSampleRange());
				int yCoord = (int) ((y + 0.5) * Params.getWifiWeightSampleRange());
				Point point = new Point(xCoord, yCoord);
				
				int numberOfParticlesToGenerateForPoint = (int) (score/sumScore * numParticlesToGenerate);
				for (int i = 0; i < numberOfParticlesToGenerateForPoint; i++){
					Particle particle;
					try {
						Particle referenceParticle = getRandomParticle();
						particle = new Particle(point.x, point.y, referenceParticle);
					} catch (IndexOutOfBoundsException e) {
						// If there are no particles left, then we have no reference particle to work from...
						particle = new Particle(point.x, point.y);
					}
					particles.add(particle);
				}
				pointNumber++;
			}
		}
		
		return particles;
	}
	
	public synchronized void stepOccured(float bearing){
		for (Particle p : particles) {
			p.moveStep(bearing);
		}
	}
	
	public class sensedLocations {
		int x;
		int y;
		double score;
		
		public sensedLocations(int x, int y, double score) {
			super();
			this.x = x;
			this.y = y;
			this.score = score;
		}
		public int getX() {
			return x;
		}
		public int getY() {
			return y;
		}
		public double getScore() {
			return score;
		}
	}
	
	public synchronized void senseOccured(List<FingerprintScore> matches){
		
		if (isFirstSense){
			initializeParticles(matches);
			isFirstSense = false;
			return;
		}
		
		List<Particle> particlesCopy = new Vector<Particle>(particles); // hack: copy the vector to prevent concurrentmodificationexception
		
		List<Double> weight = new Vector<Double>();
		for (Particle p : particlesCopy) {			
			
			// Calculate weight of each particle
			double sensedWeight = p.senseWithMeasurement(matches);
			weight.add(sensedWeight);
		}
		
		List<Particle> newParticles = resampleParticles(particles, weight, (int) (particleNum*Params.PROPORTION_PARTICLES_FROM_PREV_STATE)); // particleNum*X where X is the proportion given to motion sensor
		particles.clear();
		particles.addAll(newParticles);
		
		// TODO: add back particles
		/*for (Point point : matches) {
			Particle p = new Particle(point.x, point.y);
			particles.add(p);
		}*/
		
		int particlesMissing = particleNum - particles.size();
		
		Vector<Particle> wifiParticles = createParticlesWithDistributionOfFingerprints(matches, particlesMissing);
		particles.addAll(wifiParticles);
	}

	private synchronized List<Particle> resampleParticles(List<Particle> oldParticles, List<Double> weight, int particleNum) {
		
		List<Particle> newParticles = new Vector<Particle>();
		
		double sumWeights = 0;
		for (double w : weight) {
			sumWeights += w;
		}
		
		for (int i = 0; i < particleNum; i++) {
			double r = Math.random() * sumWeights;
			double sumWeightsSoFar = 0;
			int pNumber = 0;
			
			while (sumWeightsSoFar < r) { // in case the number of particles is not exactly the number specified
				sumWeightsSoFar += weight.get(pNumber);
				pNumber++;
			}
			
			if (pNumber > 0 && pNumber - 1 < oldParticles.size()) { // Sanity check
				Particle oldP = oldParticles.get(pNumber - 1);
				Particle newP = new Particle(oldP);
				
				newParticles.add(newP);
			}
		}
		
		return newParticles;		
	}
	
	public synchronized List<Point> getParticlesAsPoints(){
		List<Point> points = new Vector<Point>();
		for (Particle part : particles) {
			Point point = new Point();
			point.x = (int) part.getX();
			point.y = (int) part.getY();
			points.add(point);
		}
		
		return points;		
	}
	
	public synchronized Point getCenter(){
		double sumX = 0;
		double sumY = 0;
		for (Particle part : particles) {
			sumX += part.getX();
			sumY += part.getY();
		}
		
		Point point = new Point();
		point.x = (int) (sumX / particles.size());
		point.y = (int) (sumY / particles.size());
		return point;
	}	
	
	public synchronized Point getPointMinimizingError(){
		
		// Optimization: don't re-check particles that are very close together
		boolean hasBeenChecked[][] = new boolean[roomWidth][roomHeight];		
		
		Point minErrorPoint = new Point();
		double minError = Double.MAX_VALUE;
		
		for (Particle par1 : particles) {
			Point p1 = new Point( (int)par1.getX(), (int)par1.getY());
			if (hasBeenChecked[p1.x][p1.y]){
				continue;
			}
			
			PriorityQueue<Double> priorityQueue = new PriorityQueue<Double>();
			for (Particle par2 : particles){
				Point p2 = new Point( (int)par2.getX(), (int)par2.getY());
				double distance = p1.distance(p2);
				priorityQueue.add(distance);
			}
			
			// Use only PROPORTION_PARTICLES_USED_TO_DETERMINE_CENTER * no of particles to determine center
			int numParticlesToUse = (int) (Params.PROPORTION_PARTICLES_USED_TO_DETERMINE_CENTER * particles.size());
			double error = 0;
			for (int i = 0; i < numParticlesToUse; i++) {
				error += priorityQueue.poll();
			}
			
			hasBeenChecked[p1.x][p1.y] = true;
			
			if (error < minError) {
				minError = error;
				minErrorPoint = p1;
			}
		}
		
		return minErrorPoint;
		
	}
	
	public synchronized Point getPointMinimizingErrorNotOptimized(){
		// Tried to do some optimization, but not as smart as the Java compiler/JVM!
		
		// Optimization: don't re-check particles that are very close together
		boolean hasBeenChecked[][] = new boolean[roomWidth][roomHeight];
		int size = particles.size();
		int particlesX[] = new int[size];
		int particlesY[] = new int[size];
		
		Point minErrorPoint = new Point();
		double minError = Double.MAX_VALUE;
		
		int i = 0, j = 0;
		
		for (Particle particle : particles) {
			particlesX[i] = (int) particle.getX();
			particlesY[i] = (int) particle.getY();
			i++;
		}
		
		for (i = 0; i < size; i++) {
			int p1x = particlesX[i];
			int p1y = particlesY[i];
			
			double distanceSq[] = new double[size];
			for (j = 0; j < size; j++) {
				int p2x = particlesX[j];
				int p2y = particlesY[j];
				distanceSq[j] = (p1x-p2x)*(p1x-p2x) + (p1y-p2y)*(p1y-p2y);
			}
			
			Arrays.sort(distanceSq);
			
			// Use only PROPORTION_PARTICLES_USED_TO_DETERMINE_CENTER * no of particles to determine center
			int numParticlesToUse = (int) (Params.PROPORTION_PARTICLES_USED_TO_DETERMINE_CENTER * size);
			double error = 0;
			for (j = 0; j < numParticlesToUse; j++) {
				error += Math.sqrt(distanceSq[j]);
			}
			
			hasBeenChecked[p1x][p1y] = true;
			
			if (error < minError) {
				minError = error;
				minErrorPoint = new Point(p1x, p1y);
			}
		}
		
		return minErrorPoint;
		
	}
	
	public synchronized Point getPathPointMinimizingError(){
		
		Point minErrorPoint = new Point();
		double minError = Double.MAX_VALUE;
		
		BufferedImage pathImage = null;
		try {
			pathImage = ImageIO.read(new File(Params.FILE_FLOOR_PLAN_IMG));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for (int x = 0; x < roomWidth; x++) {
			for (int y = 0; y < roomHeight; y++) {
				
				Color color = new Color(pathImage.getRGB(x, y));				
				boolean isCloseToBlack = color.getRed() < 5 && color.getBlue() < 5 && color.getGreen() < 5;
				if (isCloseToBlack) {			
				
					Point point = new Point(x, y);
					
					PriorityQueue<Double> priorityQueue = new PriorityQueue<Double>();
					for (Particle particle : particles){
						double distance = point.distance(particle.getX(), particle.getY());
						priorityQueue.add(distance);
						// for some reason, using powers > 1 for the distance just worsens things
					}
					
					// Use only PROPORTION_PARTICLES_USED_TO_DETERMINE_CENTER * no of particles to determine center
					int numParticlesToUse = (int) (Params.PROPORTION_PARTICLES_USED_TO_DETERMINE_CENTER * particles.size());
					double error = 0;
					for (int i = 0; i < numParticlesToUse; i++) {
						error += priorityQueue.poll();
					}
					
					if (error < minError) {
						minError = error;
						minErrorPoint = point;
					}					
					
				}
			}
		}
		
		return minErrorPoint;
	}

	public void filterParticlesOutsidePermittedRegion(
			BufferedImage permittedRegionImage) {

		Iterator<Particle> iter = particles.iterator();
		while(iter.hasNext()){
			Particle p = iter.next();
			if (p.getX() < 0 || p.getY() < 0 || p.getX() >= permittedRegionImage.getWidth() || p.getY() >= permittedRegionImage.getHeight()) {
				iter.remove();
				continue;
			}
			if (new Color(permittedRegionImage.getRGB((int) p.getX(), (int) p.getY())).equals(Color.MAGENTA)) {
				iter.remove();
			}
		}
		
		/*
		// Resample the existing particles
		if (particles.size() < 0.1 * particleNum) {
			for (int i = 0; i < particleNum - particles.size(); i++) {
				Particle p = new Particle(0, 0);
				p.randomize(roomWidth, roomHeight);
				particles.add(p);
			}
		}

		// We need to do this to prevent the particles from "dying out"
		
		particles = resampleParticles(particles);
		*/
		
		
	}
	
	public List<Particle> resampleParticles(List<Particle> oldParticles) {
		List<Particle> newParticles = new Vector<Particle>();
		
		for (int i = 0; i < particleNum; i++) {
			double r = Math.random() * oldParticles.size();
			int pNumber = (int) r;
			
			Particle oldP = oldParticles.get(pNumber);
			Particle newP = new Particle(oldP);
				
			newParticles.add(newP);
		}
		
		return newParticles;
	}
	
	public Particle getRandomParticle() throws IndexOutOfBoundsException{		
		synchronized (particles) {
			if (particles.size() == 0) {
				throw new IndexOutOfBoundsException("Cannot get random particle from zero particles!");
			}else if (particles.size() == 1) {
				return particles.get(0);
			}else{
				int particleNo = new Random().nextInt(particles.size());
				return particles.get(particleNo);
			}
		}
	}

	public List<Particle> getParticles() {
		return particles;
	}
}
