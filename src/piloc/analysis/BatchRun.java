package piloc.analysis;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import piloc.common.ArrayStats;

public class BatchRun {
	
	public static String SOURCE_FOLDER = "";
	public static String OUTPUT_FOLDER = "";
	
	public static String GRAPH_TITLE = "";
	public static String GRAPH_TITLE_IGNORE = "";
	
	//public static final double[] PROPORTION = {1, 0.99, 0.95, 0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2, 0.1, 0.05, 0.01, 0};
	public static final double[] PROPORTION = {0.9};
	
	public static final int IGNORE_STARTING_N_LINES = 5;
	
	public static int[] NUMPARTICLES = {500, 1000, 1500, 2000, 3000, 4000, 5000};
	
	public static void main(String args[]) throws IOException {
		
		NUMPARTICLES = new int[]{1000};
		configureForCOM1B1();
		process();
		//NUMPARTICLES = new int[]{3000};
		//configureForCOM1L1();
		//process();
		//NUMPARTICLES = new int[]{1500};
		//configureForI3L3();
		//process();
		//NUMPARTICLES = new int[]{5000};
		//configureForCreateL2();
		//process();
	}

	public static void process() throws IOException, FileNotFoundException {
		
		File f = new File(SOURCE_FOLDER);
		File[] inputTraceFiles = f.listFiles();
		
		makeFolderIfNotExists(OUTPUT_FOLDER);
		BufferedWriter wSummary = new BufferedWriter(new FileWriter(OUTPUT_FOLDER+"/summaryStats.txt"));
				
		//npn for (int i = 0; i < PROPORTION.length; i++) {
		//npn 	double proportion = PROPORTION[i];
		
		for (int i = 0; i < NUMPARTICLES.length; i++) {
			double proportion = PROPORTION[0];
			int numParticles = NUMPARTICLES[i];
			
			String folderNameParams = String.format("%.2f %d", proportion, numParticles);
			String outputFolderWithParams = OUTPUT_FOLDER + "/" + folderNameParams;
			makeFolderIfNotExists(outputFolderWithParams);
			
			BufferedWriter w = new BufferedWriter(new FileWriter(outputFolderWithParams+"/combinederrorstats.txt"));
			BufferedWriter wIgnore = new BufferedWriter(new FileWriter(outputFolderWithParams+"/combinederrorstatswithignore.txt"));
			
			List<Double> wifiErrorList = new LinkedList<Double>();
			List<Double> algoErrorList = new LinkedList<Double>();
			List<Double> wifiErrorWithIgnoreList = new LinkedList<Double>();
			List<Double> algoErrorWithIgnoreList = new LinkedList<Double>();
			int wifiHistogramBins[] = new int[11];
			int algoHistogramBins[] = new int[11];
			int wifiWithIgnoreHistogramBins[] = new int[11];
			int algoWithIgnoreHistogramBins[] = new int[11];
			
			for (int j = 0; j < inputTraceFiles.length; j++) {
				
				int individualWifiHistogramBins[] = new int[11];
				int individualAlgoHistogramBins[] = new int[11];
				int individualWifiWithIgnoreHistogramBins[] = new int[11];
				int individualAlgoWithIgnoreHistogramBins[] = new int[11];
				
				File traceFile = inputTraceFiles[j];
				Params.FILE_INPUT_LOG = traceFile.getAbsolutePath();
				String traceFileName = traceFile.getName().split("\\.")[0];
				Params.FILE_OUTPUT_DIR = outputFolderWithParams + "/" + traceFileName;
				System.out.println(Params.FILE_OUTPUT_DIR);				
				makeFolderIfNotExists(Params.FILE_OUTPUT_DIR);
				
				Params.PROPORTION_PARTICLES_FROM_PREV_STATE = proportion;
				Params.NUM_PARTICLES = numParticles;
				
				// Run algorithm
				try{
					Main.process();
				}catch(Exception e){
					e.printStackTrace();
				}					
					
				// Read errorstats and populate histogramBins
				
				BufferedReader r = new BufferedReader(new FileReader(Params.FILE_OUTPUT_DIR+"/errorstats.txt"));
				String line;
				int lineCount = 0;
				w.write(traceFileName);
				w.newLine();
				wIgnore.write(traceFileName);
				wIgnore.newLine();
				
				while ((line = r.readLine()) != null){
					
					String[] lineParts = line.split("\t");
					
					double wifiError = Double.parseDouble(lineParts[3]);
					wifiErrorList.add(wifiError);
					if (wifiError < 10*Params.PIXELS_PER_METER) {
						individualWifiHistogramBins[(int) Math.floor(wifiError/Params.PIXELS_PER_METER)]++;
						wifiHistogramBins[(int) Math.floor(wifiError/Params.PIXELS_PER_METER)]++;
					} else {
						individualWifiHistogramBins[10]++;
						wifiHistogramBins[10]++;
					}
					
					double algoError = Double.parseDouble(lineParts[5]);
					algoErrorList.add(algoError);
					if (algoError < 10*Params.PIXELS_PER_METER) {
						individualAlgoHistogramBins[(int) Math.floor(algoError/Params.PIXELS_PER_METER)]++;
						algoHistogramBins[(int) Math.floor(algoError/Params.PIXELS_PER_METER)]++;
					}else{
						individualAlgoHistogramBins[10]++;
						algoHistogramBins[10]++;
					}
					
					w.write(line);
					w.newLine();

					if (lineCount >= IGNORE_STARTING_N_LINES) {
						
						wifiErrorWithIgnoreList.add(wifiError);
						if (wifiError < 10*Params.PIXELS_PER_METER) {
							individualWifiWithIgnoreHistogramBins[(int) Math.floor(wifiError/Params.PIXELS_PER_METER)]++;
							wifiWithIgnoreHistogramBins[(int) Math.floor(wifiError/Params.PIXELS_PER_METER)]++;
						} else {
							individualWifiWithIgnoreHistogramBins[10]++;
							wifiWithIgnoreHistogramBins[10]++;
						}
						
						algoErrorWithIgnoreList.add(algoError);
						if (algoError < 10*Params.PIXELS_PER_METER) {
							individualAlgoWithIgnoreHistogramBins[(int) Math.floor(algoError/Params.PIXELS_PER_METER)]++;
							algoWithIgnoreHistogramBins[(int) Math.floor(algoError/Params.PIXELS_PER_METER)]++;
						} else {
							individualAlgoWithIgnoreHistogramBins[10]++;
							algoWithIgnoreHistogramBins[10]++;
						}
						
						//wifiWithIgnoreHistogramBins[(int) Math.floor(wifiError/Params.PIXELS_PER_METER)]++;
						//algoWithIgnoreHistogramBins[(int) Math.floor(algoError/Params.PIXELS_PER_METER)]++;
						wIgnore.write(line);
						wIgnore.newLine();
					}
					lineCount++;
				}
				
				new GraphPlotter(String.format(GRAPH_TITLE, proportion),
						individualWifiHistogramBins, individualAlgoHistogramBins,
						Params.FILE_OUTPUT_DIR + "/chart.png");
				new GraphPlotter(String.format(GRAPH_TITLE_IGNORE, proportion, IGNORE_STARTING_N_LINES),
						individualWifiWithIgnoreHistogramBins, individualAlgoWithIgnoreHistogramBins,
						Params.FILE_OUTPUT_DIR + "/chart-ignore.png");
				
				r.close();
			}
			
			// Consolidate error stats
			w.close();
			wIgnore.close();
			
			logAndDisplay(wSummary, String.format("Proportion\t%f\tNum Particles\t%d", proportion, numParticles));
			logAndDisplay(wSummary, "Wifi-only Error");
			logAndDisplay(wSummary, summaryErrorStats(wifiErrorList));
			logAndDisplay(wSummary, "Algo Error");
			logAndDisplay(wSummary, summaryErrorStats(algoErrorList));
			logAndDisplay(wSummary, "Wifi with Ignore Error");
			logAndDisplay(wSummary, summaryErrorStats(wifiErrorWithIgnoreList));
			logAndDisplay(wSummary, "Algo with Ignore Error");
			logAndDisplay(wSummary, summaryErrorStats(algoErrorWithIgnoreList));
			
			// Generate chart
			new GraphPlotter(String.format(GRAPH_TITLE, proportion),
					wifiHistogramBins, algoHistogramBins,
					outputFolderWithParams + "/chart.png");
			new GraphPlotter(String.format(GRAPH_TITLE_IGNORE, proportion, IGNORE_STARTING_N_LINES),
					wifiWithIgnoreHistogramBins, algoWithIgnoreHistogramBins,
					outputFolderWithParams + "/chart-ignore.png");
			
			// Generate copy of chart in main folder
			new GraphPlotter(String.format(GRAPH_TITLE, proportion),
					wifiHistogramBins, algoHistogramBins,
					OUTPUT_FOLDER + "/" + folderNameParams + "-chart.png");
			new GraphPlotter(String.format(GRAPH_TITLE_IGNORE, proportion, IGNORE_STARTING_N_LINES),
					wifiWithIgnoreHistogramBins, algoWithIgnoreHistogramBins,
					OUTPUT_FOLDER + "/withignore-" + folderNameParams + "-chart-ignore.png");
		}
		
		wSummary.close();
	}

	public static String summaryErrorStats(List<Double> errorList) {
		return String.format(
			"Avg:\t%f%nMedian:\t%f%nAvg (m)\t%f%nMedian (m):\t%f%n75th Percentile:\t%f%n90th Percentile:\t%f%n%s%n",
			ArrayStats.getAverage(errorList), 
			ArrayStats.getMedian(errorList), 
			ArrayStats.getAverage(errorList) / Params.PIXELS_PER_METER, 
			ArrayStats.getMedian(errorList) / Params.PIXELS_PER_METER,
			ArrayStats.getPercentile(75, errorList) / Params.PIXELS_PER_METER, 
			ArrayStats.getPercentile(90, errorList) / Params.PIXELS_PER_METER,
			ArrayStats.getHistogramBucketString(errorList, Params.PIXELS_PER_METER));
	}

	public static void makeFolderIfNotExists(String folder)
			throws IOException {
		File outputFolder = new File(folder);
		if (!outputFolder.exists()){
			Files.createDirectories(outputFolder.toPath());
		}
	}
	
	private static String getOutputFolderParams() {
		String date = "22Mar";
		String proportion = "proportion="+Params.PROPORTION_PARTICLES_FROM_PREV_STATE;
		String numparticles = "nparticles="+Params.NUM_PARTICLES;
		
		String wifiMode;
		if (Params.LIMIT_WIFI_SCAN_CHANNELS.length == 0 && Params.LIMIT_WIFI_SCAN_SSID.length == 0) {
			wifiMode = "wifiall";
		}else{
			String arrayStr = Arrays.deepToString(Params.LIMIT_WIFI_SCAN_CHANNELS);
			arrayStr += Arrays.deepToString(Params.LIMIT_WIFI_SCAN_SSID);
	    	wifiMode = arrayStr;
		}
		
		//npn return String.format(" %s %s %s", date, numparticles, wifiMode);
		return String.format(" %s %s %s", date, proportion, wifiMode);
		
	}
	
	public static void logAndDisplay(BufferedWriter w, String s) {
		try {
			w.write(s);
			w.newLine();
			w.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(s);
	}
	
	public static void configureForCOM1B1(){
		Params.NUM_PARTICLES = 1000;
		
		SOURCE_FOLDER = "C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 11 Mar 2015/com1 b1/wifi slow";
		OUTPUT_FOLDER = "batchoutput com1b1";
		OUTPUT_FOLDER += getOutputFolderParams();
		
		Params.FILE_RADIO_MAP = "C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 11 Mar 2015/com1 b1/radiomap/radiomap.txt";//"gen radio map.txt"; //"com1 basement radio map.txt";
		Params.FILE_POSITION_MAP = "com1b1/posmap.txt";
		Params.FILE_FLOOR_PLAN_IMG = "com1b1/com1b1.png";
		Params.FILE_BACKGROUND_IMG = "com1b1/com1b1numbered.png";
		Params.FILE_FLOOR_PLAN_MASK_IMG = "com1b1/com1b1mask.png";
		
		Params.PIXELS_PER_METER = 16;
		Params.MAP_ANGLE_OFFSET = -51;
		
		GRAPH_TITLE = "COM1B1 (slow WiFi) proportion=%f";
		GRAPH_TITLE_IGNORE = "COM1B1 (slow WiFi) proportion=%f ignoring first %d";
	}

	public static void configureForCOM1L1(){
		Params.NUM_PARTICLES = 1000;
		
		SOURCE_FOLDER = "C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 18 Mar 2015/com1l1/wifi slow";
		OUTPUT_FOLDER = "batchoutput com1l1";
		OUTPUT_FOLDER += getOutputFolderParams();
		
		Params.FILE_RADIO_MAP = "C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 18 Mar 2015/com1l1/radiomap/radiomap.txt";//"gen radio map.txt"; //"com1 basement radio map.txt";
		Params.FILE_POSITION_MAP = "com1l1/posmap.txt";
		Params.FILE_FLOOR_PLAN_IMG = "com1l1/com1l1path.png";
		Params.FILE_BACKGROUND_IMG = "com1l1/COM1_L1.jpg";
		Params.FILE_FLOOR_PLAN_MASK_IMG = "com1l1/com1l1mask.png";
		
		Params.PIXELS_PER_METER = 8.2f;
		Params.MAP_ANGLE_OFFSET = -50;
		
		GRAPH_TITLE = "COM1L1 (slow WiFi) proportion=%f";
		GRAPH_TITLE_IGNORE = "COM1L1 (slow WiFi) proportion=%f ignoring first %d";
	}
	
	public static void configureForI3L3(){
		Params.NUM_PARTICLES = 1000; // 3000;
		
		SOURCE_FOLDER = "C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 11 Mar 2015/i3 lvl3/wifi slow";
		OUTPUT_FOLDER = "batchoutput i3lvl3";
		OUTPUT_FOLDER += getOutputFolderParams();
		
		Params.FILE_RADIO_MAP = "C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 11 Mar 2015/i3 lvl3/radiomap/radiomap.txt";//"gen radio map.txt"; //"com1 basement radio map.txt";
		Params.FILE_POSITION_MAP = "i3/posmap.txt";
		Params.FILE_FLOOR_PLAN_IMG = "i3/icubeL3.png";
		Params.FILE_BACKGROUND_IMG = "i3/icubeL3background.png";
		Params.FILE_FLOOR_PLAN_MASK_IMG = "i3/icubel3mask.png";
		
		Params.PIXELS_PER_METER = 8.6f;
		Params.MAP_ANGLE_OFFSET = -20;
		
		GRAPH_TITLE = "I3L3 (slow WiFi) proportion=%f";
		GRAPH_TITLE_IGNORE = "I3L3 (slow WiFi) proportion=%f ignoring first %d";
	}
	
	public static void configureForCreateL2(){
		Params.NUM_PARTICLES = 1000; // 5000;
		
		SOURCE_FOLDER = "C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 20 Mar 2015/createl2/wifi slow";
		OUTPUT_FOLDER = "batchoutput createl2";
		OUTPUT_FOLDER += getOutputFolderParams();
		
		Params.FILE_RADIO_MAP = "C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 20 Mar 2015/createl2/radiomap/radiomap.txt";//"gen radio map.txt"; //"com1 basement radio map.txt";
		Params.FILE_POSITION_MAP = "createl2/posmap.txt";
		Params.FILE_FLOOR_PLAN_IMG = "createl2/path.png";
		Params.FILE_BACKGROUND_IMG = "createl2/background.png";
		Params.FILE_FLOOR_PLAN_MASK_IMG = "createl2/mask.png";
		
		Params.PIXELS_PER_METER = 13.25f;
		Params.MAP_ANGLE_OFFSET = 23.36f;
		
		GRAPH_TITLE = "CreateL2 (slow WiFi) proportion=%f";
		GRAPH_TITLE_IGNORE = "CreateL2 (slow WiFi) proportion=%f ignoring first %d";
	}

}
