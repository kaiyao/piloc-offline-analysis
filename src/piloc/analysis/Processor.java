package piloc.analysis;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javax.imageio.ImageIO;

import piloc.common.Fingerprint;
import piloc.common.FingerprintScore;
import piloc.common.PositionIdMap;
import piloc.common.RadioMap;

public class Processor {
	
	ParticleFilter pf;
	RadioMap radioMap;
	
	int counter = 0;
	float currentAngle = 0;
	int positionNumber = 0;
	Point lastEstimatedPosition = null;
	Point lastWifiEstimatedPosition = null;
	private BufferedWriter errorStatsFileWriter;
	private BufferedWriter particlesFileWriter;
	
	PositionIdMap positionIdMap;
	
	private BufferedImage backgroundImg;
	private BufferedImage floorPlanImg;
	private BufferedImage maskImg;
	
	private List<Point> stepsOnlyPoints = new LinkedList<Point>();
	private Point stepsOnlyPoint = new Point(0,0);
	
	public Processor() {
		radioMap = new RadioMap(Params.FILE_RADIO_MAP);
		
		try {
			errorStatsFileWriter = new BufferedWriter(new FileWriter(Params.FILE_OUTPUT_DIR+"/"+"errorstats.txt"));
			particlesFileWriter = new BufferedWriter(new FileWriter(Params.FILE_OUTPUT_DIR+"/"+"stepLength.txt"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		positionIdMap = new PositionIdMap(Params.FILE_POSITION_MAP);
		
		try {
			backgroundImg = ImageIO.read(new File(Params.FILE_BACKGROUND_IMG));
			floorPlanImg = ImageIO.read(new File(Params.FILE_FLOOR_PLAN_IMG));
			maskImg = ImageIO.read(new File(Params.FILE_FLOOR_PLAN_MASK_IMG));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		pf = new ParticleFilter(floorPlanImg.getWidth(), floorPlanImg.getHeight());
	}
	
	public void onWifiScanReceived(String timeStamp, Fingerprint fp){
		
		List<FingerprintScore> matches = radioMap.getWeightedFpMatchByScore(fp);
		pf.senseOccured(matches);	
		
		logParticles("WIFI");
		
		if (lastWifiEstimatedPosition == null) {
			Fingerprint mostLikelyFingerprint = matches.get(matches.size()-1).getFingerprint();
			lastWifiEstimatedPosition = new Point(mostLikelyFingerprint.getX(), mostLikelyFingerprint.getY());
			recordPositionIfDataAvailable();
		}		
		
		double maxScore = 0;
		for (FingerprintScore match : matches) {
			if (match.getScore() > maxScore) {
				maxScore = match.getScore();
			}
		}
		
		if (Params.RENDER_IMAGES) {
			try {
				BufferedImage img = copyImage(backgroundImg);
				Graphics2D graphic = img.createGraphics();
				
				graphic.setFont(new Font("Tahoma", Font.PLAIN, 11)); 
				
				int fpTotalCount = matches.size();
				int fpCount = 0;
				for (FingerprintScore match : matches) {
					if (fpCount > fpTotalCount - 10) {
						graphic.setColor(new Color(1.0f, 0.0f, 0.0f, (float) (match.getScore()/maxScore)));					
					}else{
						graphic.setColor(new Color(0.0f, 0.0f, 1.0f, (float) (match.getScore()/maxScore)));
					}
					fpCount++;
					graphic.drawString(String.format("%d", (int)(match.getScore()*10000)), match.getFingerprint().getX()-5, match.getFingerprint().getY()+5);
					//graphic.fillOval(match.getFingerprint().getX()-5, match.getFingerprint().getY()-5, 10, 10);
				}
				
				graphic.setColor(Color.black);
				graphic.drawString(String.format("%d %s Pos:%d Wifi", counter, timeStamp, positionNumber), 10, 10);
	
				ImageIO.write(img, "PNG", new File(Params.FILE_OUTPUT_DIR+"/"+counter+" wifi.png"));
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		System.out.println(counter);
		counter++;
	}
	
	public void onStepOccurred(String timeStamp) {
		pf.stepOccured((currentAngle + 360 + Params.MAP_ANGLE_OFFSET) % 360);

		pf.filterParticlesOutsidePermittedRegion(maskImg);
		
		logParticles("STEP");
		if (Params.RENDER_STEPS_ONLY_IMAGE && positionNumber > 0) {
			double bearing = (currentAngle + 360 + Params.MAP_ANGLE_OFFSET) % 360;
			double bearingInRadians = bearing * Math.PI / 180.0;
			float distance = Params.STEPS_ONLY_STEP_LENGTH_IN_PIXELS;
			float newX = (float) (stepsOnlyPoint.getX() + distance * Math.sin(bearingInRadians));
			float newY = (float) (stepsOnlyPoint.getY() - distance * Math.cos(bearingInRadians));
			
			stepsOnlyPoint = new Point();
			stepsOnlyPoint.setLocation(newX, newY);
			stepsOnlyPoints.add(stepsOnlyPoint);
		}
		
		/*if (lastEstimatedPosition == null) {
			lastEstimatedPosition = center;
			recordPositionIfDataAvailable();
		}*/
		
		if (Params.RENDER_IMAGES) {
			try {
				BufferedImage img = copyImage(backgroundImg);
				Graphics2D graphic = img.createGraphics();
				
				graphic.setFont(new Font("Tahoma", Font.PLAIN, 11)); 
				
				graphic.setColor(new Color(0.0f, 0.0f, 1.0f, 0.02f));
				for (Point p : pf.getParticlesAsPoints()) {
					graphic.fillOval((int)(p.getX()-5), (int)(p.getY()-5), 10, 10);
				}
				
				graphic.setColor(new Color(0.0f, 1.0f, 0.0f, 1.0f));				
				
				lastEstimatedPosition = estimateCenterPointOfParticles();
				if (lastEstimatedPosition != null) {
					//lastEstimatedPosition = estimateCenterPointOfParticles();
					Point center = lastEstimatedPosition;
					graphic.fillOval((int)(center.getX()-5), (int)(center.getY()-5), 10, 10);
					lastEstimatedPosition = null;
				}
				
				graphic.setColor(Color.black);
				graphic.drawString(String.format("%d %s Pos:%d Step", counter, timeStamp, positionNumber), 10, 10);
				
				ImageIO.write(img, "PNG", new File(Params.FILE_OUTPUT_DIR+"/"+counter+" step.png"));
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		System.out.println(counter);
		counter++;
	}

	public Point estimateCenterPointOfParticles() {
		Point center;
		switch (Params.CENTER_METHOD) {
		case MOST_CENTER_PARTICLE:
			center = pf.getPointMinimizingError();
			break;
		case MOST_CENTER_POINT_ON_PATH:
			center = pf.getPathPointMinimizingError();
			break;
		default:
			center = pf.getPointMinimizingError();
		}
		return center;
	}
	
	public void onCompassDataReceived(String timeStamp, float mCurrentAngle) {
		currentAngle = mCurrentAngle;
	}

	public void onPositionRecordOccurred(String timeStamp, int positionNumber) {
		
		logParticles("POSITIONID " + positionNumber);
		if (Params.RENDER_STEPS_ONLY_IMAGE && this.positionNumber == 0) {
			// Is first position record received
			stepsOnlyPoint.setLocation(positionIdMap.getLocationOfId(positionNumber));
		}
		
		// TODO Auto-generated method stub
		this.positionNumber = positionNumber;
		resetRecordPositionData();
	}
	
	public void resetRecordPositionData(){
		lastEstimatedPosition = null;
		lastWifiEstimatedPosition = null;
	}
	
	public void recordPositionIfDataAvailable(){
		if (positionIdMap.getLocationOfId(positionNumber) == null || this.lastWifiEstimatedPosition == null) {
			return;
		}
		
		if (lastEstimatedPosition == null) return;
		//lastEstimatedPosition = estimateCenterPointOfParticles();
		
		Point actualPosCoords = positionIdMap.getLocationOfId(positionNumber);
		Point estimatedPosCoords = lastEstimatedPosition;
		double estDistanceError = actualPosCoords.distance(estimatedPosCoords);
		Point wifiPosCoords = lastWifiEstimatedPosition;
		double wifiDistanceError = actualPosCoords.distance(wifiPosCoords);
		
		try {
			errorStatsFileWriter.write(String.format("%d\t%d,%d\t%d,%d\t%f\t%d,%d\t%f", 
					positionNumber,	
					actualPosCoords.x, actualPosCoords.y,
					wifiPosCoords.x, wifiPosCoords.y,
					wifiDistanceError,
					estimatedPosCoords.x, estimatedPosCoords.y,						
					estDistanceError));
			errorStatsFileWriter.newLine();
			errorStatsFileWriter.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	void logParticles(String extra){
		if (!Params.LOG_PARTICLES) {
			return;
		}
		List<Particle> particles = pf.getParticles();
		try {
			particlesFileWriter.write(String.format("* %d %s", counter, extra));
			particlesFileWriter.newLine();
			for (Particle particle : particles) {
				particlesFileWriter.write(String.format("%f\t%f\t%f", particle.getX(), particle.getY(), particle.getStepLength()));
				particlesFileWriter.newLine();
			}
			particlesFileWriter.newLine();
			//particlesFileWriter.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	static BufferedImage copyImage(BufferedImage bi) {
		/*ColorModel cm = bi.getColorModel();
		boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
		WritableRaster raster = bi.copyData(null);
		return new BufferedImage(cm, raster, isAlphaPremultiplied, null);*/
		return new BufferedImage(bi.getWidth(), bi.getHeight(), BufferedImage.TYPE_INT_ARGB);
	}

	public void finished() {
		// TODO Auto-generated method stub
		
		try {
			this.particlesFileWriter.close();
			this.errorStatsFileWriter.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		try {
			double minX = Double.MAX_VALUE, minY = Double.MAX_VALUE, maxX = Double.MIN_VALUE, maxY = Double.MIN_VALUE;
			for(Point p : stepsOnlyPoints){
				if (p.getX() < minX) minX = p.getX();
				if (p.getY() < minY) minY = p.getY();
				if (p.getX() > maxX) maxX = p.getX();
				if (p.getY() > maxY) maxY = p.getY();
			}
			double offsetX = 0 - minX + 10;
			double offsetY = 0 - minY + 10;
			double width = maxX - minX + 20;
			double height = maxY - minY + 20;
			
			BufferedImage stepsOnlyImg = new BufferedImage((int) width, (int) height, BufferedImage.TYPE_4BYTE_ABGR);
			Graphics2D stepsOnlyImgGraphic = stepsOnlyImg.createGraphics();			
			stepsOnlyImgGraphic.setColor(new Color(0.0f, 0.0f, 1.0f, 0.2f));
			
			for(Point p : stepsOnlyPoints){
				stepsOnlyImgGraphic.fillOval((int)(p.getX()+offsetX-5), (int)(p.getY()+offsetY-5), 10, 10);
			}
			
			ImageIO.write(stepsOnlyImg, "PNG", new File(Params.FILE_OUTPUT_DIR+"/stepsImage.png"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
