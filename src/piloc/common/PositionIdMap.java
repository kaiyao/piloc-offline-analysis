package piloc.common;
import java.awt.Point;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.TreeMap;


public class PositionIdMap {
	
	List<Point> positions;
	
	public PositionIdMap(String filename){
		positions = new ArrayList<Point>();
		positions.add(null);
		
		BufferedReader br;

		try {
			String line;
			br = new BufferedReader(new FileReader(filename));
			
			while ((line = br.readLine()) != null) {
				String[] lineParts = line.split("\t");
				int id = Integer.parseInt(lineParts[0]);
				int x = Integer.parseInt(lineParts[1]);
				int y = Integer.parseInt(lineParts[2]);
				
				Point point = new Point(x, y);
				positions.add(id, point);
			}			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Point getLocationOfId(int positionId) {
		return positions.get(positionId);
	}
	
	public int getNumberOfPositions(){
		return positions.size();
	}

}
