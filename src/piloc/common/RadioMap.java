package piloc.common;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;


public class RadioMap {
	
	List<Fingerprint> radioData;
	Set<String> accessPointSet;
	int minPointApCount = Integer.MAX_VALUE;
	int maxPointApCount = Integer.MIN_VALUE;
	
	public RadioMap(String filename){
		radioData = new ArrayList<Fingerprint>();
		accessPointSet = new TreeSet<String>();
		
		BufferedReader br;

		try {
			String line;
			br = new BufferedReader(new FileReader(filename));
			
			while ((line = br.readLine()) != null) {
				String[] lineParts = line.split(" ");
				int x = Integer.parseInt(lineParts[0]);
				int y = Integer.parseInt(lineParts[1]);
				
				Fingerprint pointFP = new Fingerprint(x, y);
				int pointApCount = 0;
				for (int i=2; i<lineParts.length; i+=2) {					
					pointFP.addFingerprint(lineParts[i], Integer.parseInt(lineParts[i+1]));
					accessPointSet.add(lineParts[i]);
					pointApCount++;
				}
				
				if (pointApCount > maxPointApCount) maxPointApCount = pointApCount;
				if (pointApCount < minPointApCount) minPointApCount = pointApCount;
				
				
				//System.out.println(String.format("%d %d %f", x, y, pointFP.getSimilarityTo(fpset)));
			
				radioData.add(pointFP); 
			}			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public class LocationScore {
		int x;
		int y;
		double score;		
		
		public LocationScore(int x, int y, double score) {
			super();
			this.x = x;
			this.y = y;
			this.score = score;
		}
		public int getX() {
			return x;
		}
		public int getY() {
			return y;
		}
		public double getScore() {
			return score;
		}
		
	}
	
	/*
	public List<LocationScore> locateOnRadioMap(Fingerprint fpToLocate) {
		SortedMap<Double, Fingerprint> map = getFpMatchByScore(fpToLocate);
		List<LocationScore> listLocationScore = new ArrayList<LocationScore>();
		for( Entry<Double, Fingerprint> mapEntry : map.entrySet()) {
			Fingerprint fp = mapEntry.getValue();
			listLocationScore.add(new LocationScore(fp.getX(), fp.getY(), mapEntry.getKey()));
		}
		return listLocationScore;
	}
	*/
	
	/**
	 * This method is not being used. Please use getWeightedFpMatchByScore instead
	 * @param fpToLocate
	 * @return
	 */
	public List<FingerprintScore> getFpMatchByScore(Fingerprint fpToLocate) {
		// Score measures difference, so lower is better!
		
		List<FingerprintScore> fpListWithScore = new ArrayList<FingerprintScore>();
		for(Fingerprint fp : radioData) {
			double score = fp.getSimilarityTo(fpToLocate);
			fpListWithScore.add(new FingerprintScore(fp, score));
		}
		Collections.sort(fpListWithScore);
		return fpListWithScore;
	}
	
	public List<FingerprintScore> getWeightedFpMatchByScore(Fingerprint fpToLocate){
		List<FingerprintScore> fpListWithScore = new ArrayList<FingerprintScore>();
		for(Fingerprint fp : radioData) {
			double score = fp.getWeightedSimilarityTo(fpToLocate);
			fpListWithScore.add(new FingerprintScore(fp, score));
		}
		Collections.sort(fpListWithScore);
		return fpListWithScore;
	}

	public Set<String> getAccessPointSet() {
		return accessPointSet;
	}

	public int getMinPointApCount() {
		return minPointApCount;
	}

	public int getMaxPointApCount() {
		return maxPointApCount;
	}
	
	
	//public List<FingerprintScore> getFpMatchByNormalizedScore(Fingerprint fpToLocate) {
		/*
		 * e.g. 30, 20, 10 (score)
		 * sum = 60
		 * 30/60 = 0.5, 20/60 = 0.33, 10/60 = 0.16
		 * 60/30 = 2, 60/20 = 3, 60/10 = 6
		 * 
		 */
	/*	
		List<FingerprintScore> fpListWithScore = getFpMatchByScore(fpToLocate);
		double sum = 0;
		for(FingerprintScore item : fpListWithScore) {
			sum += item.getScore();
		}
		
		double maxScore = 0;
		
		// Take inverse of values such that closer matches have higher score
		for(FingerprintScore item : fpListWithScore) {
			double inverseScore;
			if (item.getScore() == 0){
				inverseScore = 0;
			}else{
				inverseScore = sum/item.getScore();
			}
			item.setScore(inverseScore);
			
			if (inverseScore > maxScore) {
				maxScore = inverseScore;
			}
		}
		
		// Normalize the scores to between zero and one
		for(FingerprintScore item : fpListWithScore) {
			item.setScore(item.getScore()/maxScore);
		}		
		
		Collections.sort(fpListWithScore);
		
		return fpListWithScore;
	}*/
	
	

}
