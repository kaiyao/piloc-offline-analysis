package piloc.common;
import java.awt.Point;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Fingerprint {
	
	private Map<String, Integer> fpset;
	private int x;
	private int y;

	public Fingerprint() {
		this(0,0);
	}
	
	public Fingerprint(int x, int y) {
		this.x = x;
		this.y = y;
		fpset = new HashMap<String, Integer>();
	}
	
	public void addFingerprint(String bssid, int rssi) {
		fpset.put(bssid, rssi);
	}
	
	public Set<String> getBssids(){
		return fpset.keySet();
	}
	
	public boolean containsBssid(String bssid){
		return fpset.containsKey(bssid);
	}
	
	public int getRssi(String bssid) {
		return fpset.get(bssid);
	}
	
	public double getSimilarityTo(Fingerprint fpset2){
		
		double sum = 0;
		for (String bssid : getBssids()) {
			int rssiDiff = 0;
					
			if (fpset2.containsBssid(bssid)) {
				rssiDiff = fpset2.getRssi(bssid) - this.getRssi(bssid);
			}
			sum += rssiDiff * rssiDiff; // 0 if bssid doesn't exist in other fingerprint vector
		}
		return Math.sqrt(sum);
		
	}
	
	public double getWeightedSimilarityTo(Fingerprint fpset2){
		double sum = 0;
		
		for (String bssid : this.getBssids()) {
			int rssiDiff = 0;
			
			if (fpset2.containsBssid(bssid)) {
				rssiDiff = Math.abs(fpset2.getRssi(bssid) - this.getRssi(bssid));
				if (rssiDiff == 0) {
					sum += 1.0 / this.getRssi(bssid);
				}else{
					sum += 1.0 / this.getRssi(bssid) * 1.0/rssiDiff;
				}				
			}			
		}
		
		return sum;
		
	}

	public int getX() {
		return x;
	}

	public void setX(int x) {
		this.x = x;
	}

	public int getY() {
		return y;
	}

	public void setY(int y) {
		this.y = y;
	}
	
	public Point getPoint(){
		return new Point(x,y);
	}
	

}
