package piloc.common.utility;

import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.List;

public class NearestMatchingPixelFinder {

	BufferedImage img;	
	PixelMatchCriteria pixelMatchCriteria;	
	List<Point> validPixels;
	
	public NearestMatchingPixelFinder(BufferedImage image, PixelMatchCriteria pmc) {
		super();
		this.img = image;
		this.pixelMatchCriteria = pmc;
	}
	
	public Point getNearestValidPixel(Point givenPoint) {
		
		// http://stackoverflow.com/questions/307445/finding-closest-non-black-pixel-in-an-image-fast
		
		if (pixelMatchCriteria.checkPixelAtPositionMatchesCriteria(img, givenPoint.x, givenPoint.y)){
			return givenPoint;
		}
		
		validPixels = new LinkedList<Point>();
		
		int squareSearchDistance = 1;
		while (validPixels.isEmpty()) {
			searchSquare(squareSearchDistance, givenPoint);
			squareSearchDistance++;
		}
		
		//System.out.println(validPixels);
		
		int maxDistanceToSearch = squareSearchDistance + (int) Math.ceil(validPixels.get(0).distance(givenPoint));
		while (squareSearchDistance < maxDistanceToSearch) {
			searchSquare(squareSearchDistance, givenPoint);
			squareSearchDistance++;
		}
		
		//System.out.println(validPixels);
		
		Point nearestPoint = validPixels.get(0);
		double nearestDistanceSq = nearestPoint.distanceSq(givenPoint);
		
		for(Point p : validPixels) {
			double distanceSq = p.distanceSq(givenPoint);
			if (distanceSq < nearestDistanceSq) {
				nearestDistanceSq = distanceSq;
				nearestPoint = p;
			}
		}
		
		return nearestPoint;
	}

	public void searchSquare(int squareSearchDistance, Point aroundPoint) {
		
		Point p;
		
		// Search top and bottom rows
		int leftBound = aroundPoint.x - squareSearchDistance;
		if (leftBound < 0) leftBound = 0;
		
		int rightBound = aroundPoint.x + squareSearchDistance;
		if (rightBound >= img.getWidth()) rightBound = img.getWidth() - 1;
		
		int topRowY = aroundPoint.y - squareSearchDistance;
		if (topRowY < 0) topRowY = 0;
		
		int bottomRowY = aroundPoint.y + squareSearchDistance;
		if (bottomRowY >= img.getHeight()) bottomRowY = img.getHeight() - 1;
		
		for (int x = leftBound; x <= rightBound; x++) {
			
			// Check top row pixels
			p = new Point(x, topRowY);
			//System.out.println("Top " + p);
			if (pixelMatchCriteria.checkPixelAtPositionMatchesCriteria(img, p.x, p.y)){
				validPixels.add(p);
			}
			
			// Check bottom row pixels
			p = new Point(x, bottomRowY);
			// System.out.println("Bot " + p);
			if (pixelMatchCriteria.checkPixelAtPositionMatchesCriteria(img, p.x, p.y)) {
				validPixels.add(p);
			}
			
		}
		
		// Search left and right columns
		int topBound = aroundPoint.y - squareSearchDistance + 1;
		if (topBound < 0) topBound = 0;
		int bottomBound = aroundPoint.y + squareSearchDistance - 1;
		if (bottomBound >= img.getHeight()) bottomBound = img.getHeight() - 1;
		
		int leftCol = aroundPoint.x - squareSearchDistance;
		if (leftCol < 0) leftCol = 0;
		
		int rightCol = aroundPoint.x + squareSearchDistance;
		if (rightCol >= img.getWidth()) rightCol = img.getWidth() - 1;
		
		for (int y = topBound; y <= bottomBound; y++) {

			// Check left column pixels
			p = new Point(leftCol, y);
			//System.out.println("Lef Row" + p);
			if (pixelMatchCriteria.checkPixelAtPositionMatchesCriteria(img, p.x, p.y)) {
				validPixels.add(p);
			}

			// Check right column pixels
			p = new Point(rightCol, y);
			// System.out.println("Rig Row" + p);
			if (pixelMatchCriteria.checkPixelAtPositionMatchesCriteria(img, p.x, p.y)) {
				validPixels.add(p);
			}

		}
	}
	
}
