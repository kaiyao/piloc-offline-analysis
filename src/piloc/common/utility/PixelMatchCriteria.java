package piloc.common.utility;

import java.awt.image.BufferedImage;

public interface PixelMatchCriteria {
	
	public boolean checkPixelAtPositionMatchesCriteria(BufferedImage img, int x, int y);

}
