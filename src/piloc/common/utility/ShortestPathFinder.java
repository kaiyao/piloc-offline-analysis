package piloc.common.utility;

import java.awt.Color;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Stack;

import javax.imageio.ImageIO;

public class ShortestPathFinder {
	
	Point[][] discoveredPoints;
	Queue<Point> queue;
	BufferedImage img;
	PixelMatchCriteria pixelMatchCriteria;
	
	public ShortestPathFinder(BufferedImage pathImage, PixelMatchCriteria pmc) {
		super();
		this.img = pathImage;
		this.pixelMatchCriteria = pmc;
	}
	
	public class SPath {
		
		List<Point> pathPoints;

		public SPath(List<Point> pathPoints) {
			super();
			this.pathPoints = pathPoints;
		}
		
		public List<Point> getPath(){
			return pathPoints;
		}
		
		public double getPathLength(){
			double pathLength = 0;
			
			Point prevPoint = null;
			for (Point point : pathPoints) {
				if (prevPoint != null) {
					pathLength += prevPoint.distance(point);
				}
				prevPoint = point;
			}
			return pathLength;
		}
		
		public Point getNearestPathPoint(Point toPoint) {
			Point nearestPoint = null;
			double minDistance = Double.MAX_VALUE;
			for (Point point : pathPoints) {
				double distance = point.distanceSq(toPoint);
				if (distance < minDistance) {
					minDistance = distance;
					nearestPoint = point;
				}
			}
			
			return nearestPoint;
		}
		
		public float getBearingAtPoint(Point point, int delta) {
			Point[] pointsArr = pathPoints.toArray(new Point[pathPoints.size()]);
			for (int i = 0; i < pointsArr.length; i++) {
				if (point.equals(pointsArr[i])){
					// Note that older points have a larger index, since the path points are starting from the end
					int olderPointNum = i - delta;
					if (olderPointNum < 0) {
						olderPointNum = 0;
					}
					Point olderPoint = pointsArr[olderPointNum];

					int newerPointNum = i + delta;
					if (newerPointNum >= pointsArr.length) {
						newerPointNum = pointsArr.length - 1;
					}
					Point newerPoint = pointsArr[newerPointNum];
					
					int deltaX = olderPoint.x - newerPoint.x;
					int deltaY = olderPoint.y - newerPoint.y;
					
					double angleRad = Math.atan2(deltaY, deltaX);
					return (float) (90 - Math.toDegrees(angleRad));					
				}
			}
			return 0;
		}
		
	}

	public SPath getShortestPath(Point startPoint, Point endPoint) {
		
		discoveredPoints = new Point[img.getWidth()][img.getHeight()];
		queue = new LinkedList<Point>();
		
		//Point startPoint = new Point(545,73);
		//Point endPoint = new Point(664,246);
		boolean isPathFound = false;
		
		queue.offer(startPoint);
		
		while(!queue.isEmpty()){
			Point p = queue.poll();
			//System.out.println(p);
			if (p.equals(endPoint)) {
				isPathFound = true;
				break;
			}

			int x = p.x;
			int y = p.y;
			
			if (pixelMatchCriteria.checkPixelAtPositionMatchesCriteria(img, x, y)){				
				
				addPointToQueueIfNotYetVisited(x - 1, y - 1, x, y); // Top left
				addPointToQueueIfNotYetVisited(x    , y - 1, x, y); // Top center
				addPointToQueueIfNotYetVisited(x + 1, y - 1, x, y); // Top right
				
				addPointToQueueIfNotYetVisited(x - 1, y    , x, y); // Left
				addPointToQueueIfNotYetVisited(x + 1, y    , x, y); // Right
				
				addPointToQueueIfNotYetVisited(x - 1, y + 1, x, y); // Bottom left
				addPointToQueueIfNotYetVisited(x    , y + 1, x, y); // Bottom center
				addPointToQueueIfNotYetVisited(x + 1, y + 1, x, y); // Bottom right
				
			}
		}
		
		if (isPathFound) {
			Point currPoint = endPoint;
			while(!currPoint.equals(startPoint)) {
				// img.setRGB(currPoint.x, currPoint.y, Color.GREEN.getRGB());
				currPoint = discoveredPoints[currPoint.x][currPoint.y];
			}
			
			// ImageIO.write(img, "PNG", new File("shortestpath.png"));
		}
		
		if (isPathFound) {
			Stack<Point> stack = new Stack<Point>();
			List<Point> list = new ArrayList<Point>();
			
			Point currPoint = endPoint;
			stack.push(currPoint);
			while(!currPoint.equals(startPoint)) {
				currPoint = discoveredPoints[currPoint.x][currPoint.y];
				stack.push(currPoint);
			}
			
			// Invert the list of points
			while(!stack.isEmpty()) {
				list.add(stack.pop());
			}
			
			return new SPath(list);
		}
		
		return null;
	}

	public void addPointToQueueIfNotYetVisited(int x, int y, int predX, int predY) {
		if (!(discoveredPoints[x][y] instanceof Point)) {
			discoveredPoints[x][y] = new Point(predX, predY);
			queue.offer(new Point(x, y));
		}
	}

}
