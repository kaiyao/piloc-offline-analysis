package piloc.common;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class ReadWifiData {
	
	public static void readWifiData(BufferedReader br, String[] lineParts,
			Fingerprint fp, int limitWifiScansToMostRecentMilliseconds,
			Integer[] limitWifiScanChannels, String[] limitWifiScanSsids)
			throws IOException {
		String line;
		if (lineParts.length == 2) {
			// Process iw wifi data format
			line = br.readLine();
			String bssid = "";
			String ssid = "";
			int frequency = 0;
			int signal = 0;
			int lastSeen = 0;
			while (line != null && !line.equalsIgnoreCase("----------ENDWIFI")) {
				
				if (line.startsWith("BSS ")){
					bssid = line.substring(4, 21);
				}
				if (line.startsWith("\tfreq: ")){
					frequency = Integer.parseInt(line.substring(7, 11));
				}
				if (line.startsWith("\tsignal: ")){
					signal = (int) Math.abs(Double.parseDouble(line.substring(9, 16)));
				}
				if (line.startsWith("\tlast seen: ")){
					String lastSeenString = line.substring(12);
					String[] lastSeenStringArr = lastSeenString.split(" ");
					lastSeen = Integer.parseInt(lastSeenStringArr[0]);
				}
				if (line.startsWith("\tSSID: ")){
					ssid = line.substring(7);
					
					boolean isTimely;
					boolean isAcceptedFrequency;
					boolean isAcceptedSsid;
					
					if (lastSeen < limitWifiScansToMostRecentMilliseconds){
						isTimely = true;
					}else{
						isTimely = false;
					}
					
					if (limitWifiScanChannels.length == 0){
						isAcceptedFrequency = true;
					}else{
						List<Integer> list = Arrays.asList(limitWifiScanChannels);
						if (list.contains(frequency)){
							isAcceptedFrequency = true;
						}else{
							isAcceptedFrequency = false;
						}
					}
					
					if (limitWifiScanSsids.length == 0){
						isAcceptedSsid = true;
					}else{
						if (Arrays.asList(limitWifiScanSsids).contains(ssid)){
							isAcceptedSsid = true;
						}else{
							isAcceptedSsid = false;
						}
					}
					
					if (isTimely && isAcceptedFrequency && isAcceptedSsid) {
						fp.addFingerprint(bssid, signal);
					}
				}
				line = br.readLine();
			}
			
		}else{
			// Process linear wifi data format
			for(int i=2; i<lineParts.length; i++) {
				String[] wifiParts = lineParts[i].split(" ");
				String bssid = wifiParts[0];
				String ssid = wifiParts[1];
				String timestamp = wifiParts[2];
				String readableTimestamp = wifiParts[3]+" "+wifiParts[4];
				int frequency =  Integer.parseInt(wifiParts[5]);
				int rssi = Math.abs(Integer.parseInt(wifiParts[6])); // rssi is absoluted to match radio map
				System.out.println(bssid + " " + rssi);
				fp.addFingerprint(bssid, rssi);
			}
		}
		
	}

}
