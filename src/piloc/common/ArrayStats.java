package piloc.common;

import java.util.Arrays;
import java.util.List;

public class ArrayStats {
	
	public static double getPercentile(int percentile, List<Double> array) {
		return getPercentile(percentile, array.toArray(new Double[array.size()]));
	}
	
	public static double getPercentile(int percentile, Double[] array) {
		Arrays.sort(array);
		
		double p = percentile/100.0;
		int index = (int) Math.round(p * array.length);
		return array[index];
		
	}
	
	public static double getMedian(List<Double> array) {
		return getMedian(array.toArray(new Double[array.size()]));
	}
	
	public static double getMedian(Double[] array) {
		
		Arrays.sort(array);
		
		int size = array.length;
		
		double median;
		if (size % 2 == 0) {
			median = (array[size/2-1] + array[size/2])/2;
		}else{
			median = array[(int) size/2];
		}
		
		return median;
		
	}
	
	public static double getAverage(List<Double> array) {
		return getAverage(array.toArray(new Double[array.size()]));
	}
	
	
	public static double getAverage(Double[] array) {
		double sum = 0;
		int size = array.length;
		
		for (int i = 0; i < size; i++) {
			sum += array[i];
		}
		
		double average = sum / size;
		
		return average;
	}
	
	public static String getHistogramBucketString(List<Double> array, double pixelsPerMeter) {
		return getHistogramBucketString(array.toArray(new Double[array.size()]), pixelsPerMeter);
	}
	
	public static String getHistogramBucketString(Double[] array, double pixelsPerMeter){
		int histogramBins[] = new int[11];
		for (int i = 0; i < array.length; i++) {
			double val = array[i];
			if (val < 10 * pixelsPerMeter) {
				histogramBins[(int) Math.floor(val / pixelsPerMeter)]++;
			} else {
				histogramBins[10]++;
			}
		}
		
		StringBuffer histogramString = new StringBuffer();
		for (int j = 0; j < histogramBins.length; j++) {
			histogramString.append(histogramBins[j] + "\t");				
		}
		return histogramString.toString();
	}


}
