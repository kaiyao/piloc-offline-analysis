package piloc.common;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class FingerprintScore implements Comparable<FingerprintScore> {
	double score;
	Fingerprint fp;
	
	public FingerprintScore(Fingerprint fp, double score) {
		super();
		this.score = score;
		this.fp = fp;
	}		

	public void setScore(double score) {
		this.score = score;
	}

	public double getScore() {
		return score;
	}

	public Fingerprint getFingerprint() {
		return fp;
	}

	@Override
	public int compareTo(FingerprintScore fp2) {
		return Double.compare(this.getScore(), fp2.getScore());
	}
	
}