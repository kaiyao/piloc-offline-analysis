package piloc.analysis3a;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.imageio.ImageIO;

import piloc.common.Fingerprint;
import piloc.common.FingerprintScore;
import piloc.common.PositionIdMap;
import piloc.common.RadioMap;
import piloc.common.utility.NearestMatchingPixelFinder;
import piloc.common.utility.PixelMatchCriteria;
import piloc.common.utility.ShortestPathFinder;
import piloc.common.utility.ShortestPathFinder.SPath;

public class Processor {
	
	RadioMap radioMap;
	
	int counter = 0;
	float currentAngle = 0;
	int positionNumber = 0;
	Point lastEstimatedPosition = null;
	Point lastWifiEstimatedPosition = null;
	private BufferedWriter errorStatsFileWriter;
	int stepCount = 0;
	
	PositionIdMap positionIdMap;
	
	private BufferedImage backgroundImg;
	private BufferedImage floorPlanImg;
	private BufferedImage maskImg;
	
	private List<State> states;
	private State currentState = null;
	
	float stepLength = Params.getInitialStepLength();
	
	PixelMatchCriteria pmc = new PixelMatchCriteria(){

		@Override
		public boolean checkPixelAtPositionMatchesCriteria(
				BufferedImage img, int x, int y) {
			return new Color(img.getRGB(x, y)).equals(Color.BLACK);
		}
		
	};

	private float maxStepLength;
	
	public Processor() {
		radioMap = new RadioMap(Params.FILE_RADIO_MAP);
		
		try {
			errorStatsFileWriter = new BufferedWriter(new FileWriter(Params.FILE_OUTPUT_DIR+"/"+"errorstats.txt"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		positionIdMap = new PositionIdMap(Params.FILE_POSITION_MAP);
		
		try {
			backgroundImg = ImageIO.read(new File(Params.FILE_BACKGROUND_IMG));
			floorPlanImg = ImageIO.read(new File(Params.FILE_FLOOR_PLAN_IMG));
			maskImg = ImageIO.read(new File(Params.FILE_FLOOR_PLAN_MASK_IMG));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		states = new ArrayList<State>();

	}
	
	public void onWifiScanReceived(String timeStamp, Fingerprint fp){
		
		maxStepLength = Params.MAX_STEP_LENGTH;
		
		if (stepCount == 0){
			return;
		}
		
		if (positionNumber == 0){
			return;
		}
		
		List<FingerprintScore> matches = radioMap.getWeightedFpMatchByScore(fp);
		//renderWifiImage(timeStamp, matches, "wifi");
		
		NearestMatchingPixelFinder nmpf = new NearestMatchingPixelFinder(floorPlanImg, pmc);
		
		List<FingerprintScore> xmatches = getTopXSeparatedFingerprints(10, matches);
		
		if (lastEstimatedPosition == null) {
			lastEstimatedPosition = xmatches.get(0).getFingerprint().getPoint();
		}else{
			renderWifiImage(timeStamp, xmatches, "before-filtering-wifi");
			
			int xmatchNum = 0;
			Iterator<FingerprintScore> iter = xmatches.iterator();
			while(iter.hasNext()) {
				FingerprintScore match = iter.next();
				
				Point oldLocation = lastEstimatedPosition;
				Point newLocation = match.getFingerprint().getPoint();
				SPath shortestPath = new ShortestPathFinder(floorPlanImg, pmc).getShortestPath(
						nmpf.getNearestValidPixel(newLocation), 
						nmpf.getNearestValidPixel(oldLocation)
				);
				double pathLength = shortestPath.getPathLength();
				
				BufferedImage img = copyImage(backgroundImg);
				Graphics2D graphic = img.createGraphics();
				
				// Draw prev position
				graphic.setColor(new Color(0.0f, 0.0f, 1.0f, 0.5f));
				graphic.fillOval((int) lastEstimatedPosition.getX()-5, (int) lastEstimatedPosition.getY()-5, 10, 10);
				
				// Draw new candidate positions
				Color color = new Color(0.0f, 1.0f, 0.0f, 0.5f);
				if (pathLength > stepCount * maxStepLength * Params.PIXELS_PER_METER) {
					color = new Color(1.0f, 0.0f, 0.0f, 0.5f);
				}
			
				graphic.setColor(color);
				graphic.fillOval((int) newLocation.getX()-5, (int) newLocation.getY()-5, 10, 10);
				for (Point p : shortestPath.getPath()) {
					img.setRGB(p.x, p.y, color.getRGB());
				}
				
				if (pathLength > stepCount * maxStepLength * Params.PIXELS_PER_METER) {
					iter.remove();
				}
				
				try {
					ImageIO.write(img, "PNG", new File(Params.FILE_OUTPUT_DIR+"/"+counter+"-"+(xmatchNum++)+"-paths.png"));
					
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				
			}
			
			
			System.out.println(stepCount+" "+xmatches.size());
			
			//renderWifiImage(timeStamp, matches, "-no-filtered-wifi");
			if (xmatches.size() == 0) {
				lastEstimatedPosition = matches.get(matches.size() - 1).getFingerprint().getPoint();
			}else{
				lastEstimatedPosition = xmatches.get(0).getFingerprint().getPoint();
				renderWifiImage(timeStamp, xmatches, "filtered-wifi");
			}
		}
					
		if (lastWifiEstimatedPosition == null) {
			Fingerprint mostLikelyFingerprint = matches.get(matches.size()-1).getFingerprint();
			lastWifiEstimatedPosition = new Point(mostLikelyFingerprint.getX(), mostLikelyFingerprint.getY());
			recordPositionIfDataAvailable();
		}
		
		stepCount = 0;
		
		counter++;
		System.out.println(counter);
		
	}
	
	private float getAverageOfList(List<Float> list) {
		float sum = 0;
		
		for (Float i : list) {
			sum += i;
		}
		
		return sum / list.size();
	}

	private int calculateStepsBetweenStates(int fromState, int toState) {
		int sumSteps = 0;
		// remember that the most recent state (toState) has no steps
		// associated with it yet
		// each "state" stores the wifi fingerprint and the steps after it
		for (int i = fromState; i < toState; i++) {
			State state = states.get(i);
			sumSteps += state.getSteps().size();
		}
		return sumSteps;
	}
	
	private List<Float> getAnglesFromStepsBetweenStates(int fromState, int toState) {
		List<Float> angles = new LinkedList<Float>();
		for (int i = fromState; i < toState; i++) {
			State state = states.get(i);
			for (Step s : state.getSteps()) {
				angles.add(s.angle);
			}
		}
		return angles;
	}

	public void onStepOccurred(String timeStamp) {
		
		stepCount++;
		
		if (currentState == null) {
			return;
		}
		
		float stepAngle = (currentAngle + 360 + Params.MAP_ANGLE_OFFSET) % 360;
		
		currentState.addStep(new Step(stepLength, stepAngle));
		
		System.out.println(counter);
		counter++;
		
	}
	
	public void onCompassDataReceived(String timeStamp, float mCurrentAngle) {
		currentAngle = mCurrentAngle;
	}

	public void onPositionRecordOccurred(String timeStamp, int positionNumber) {
		
		// TODO Auto-generated method stub
		this.positionNumber = positionNumber;
		resetRecordPositionData();
	}
	
	public void resetRecordPositionData(){
		lastWifiEstimatedPosition = null;
	}
	
	public void recordPositionIfDataAvailable(){
		if (positionIdMap.getLocationOfId(positionNumber) == null || this.lastWifiEstimatedPosition == null) {
			return;
		}
		
		if (this.lastEstimatedPosition == null) {
			this.lastEstimatedPosition = new Point(0,0);
		}
		
		Point actualPosCoords = positionIdMap.getLocationOfId(positionNumber);
		Point estimatedPosCoords = lastEstimatedPosition;
		double estDistanceError = actualPosCoords.distance(estimatedPosCoords);
		Point wifiPosCoords = lastWifiEstimatedPosition;
		double wifiDistanceError = actualPosCoords.distance(wifiPosCoords);
		
		try {
			errorStatsFileWriter.write(String.format("%d\t%d,%d\t%d,%d\t%f\t%d,%d\t%f", 
					positionNumber,	
					actualPosCoords.x, actualPosCoords.y,
					wifiPosCoords.x, wifiPosCoords.y,
					wifiDistanceError,
					estimatedPosCoords.x, estimatedPosCoords.y,						
					estDistanceError));
			errorStatsFileWriter.newLine();
			errorStatsFileWriter.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	static BufferedImage copyImage(BufferedImage bi) {
		/*ColorModel cm = bi.getColorModel();
		boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
		WritableRaster raster = bi.copyData(null);
		return new BufferedImage(cm, raster, isAlphaPremultiplied, null);*/
		return new BufferedImage(bi.getWidth(), bi.getHeight(), BufferedImage.TYPE_INT_ARGB);
	}

	public void finished() {
		// TODO Auto-generated method stub
		
	}	

	private void renderImage(String timeStamp, 
			List<PointScore> locationScores, String label) {
		if (Params.RENDER_IMAGES) {

			try {
				BufferedImage img = copyImage(backgroundImg);
				Graphics2D graphic = img.createGraphics();
				
				graphic.setFont(new Font("Tahoma", Font.PLAIN, 11)); 
				
				for (PointScore ps : locationScores) {
					graphic.setColor(new Color(0.0f, 0.0f, 1.0f));
					graphic.drawString(String.format("%f", ps.getScore()), ps.getX()-5, ps.getY()+5);
					//graphic.fillOval(match.getFingerprint().getX()-5, match.getFingerprint().getY()-5, 10, 10);
				}
				
				graphic.setColor(Color.black);
				graphic.drawString(String.format("%d %s Pos:%d Wifi", counter, timeStamp, positionNumber), 10, 10);
	
				ImageIO.write(img, "PNG", new File(Params.FILE_OUTPUT_DIR+"/"+counter+" "+label+".png"));
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	private void renderWifiImage(String timeStamp, List<FingerprintScore> matches, String label) {
		if (Params.RENDER_IMAGES) {
			double maxScore = 0;
			for (FingerprintScore match : matches) {
				if (match.getScore() > maxScore) {
					maxScore = match.getScore();
				}
			}
			try {
				BufferedImage img = copyImage(backgroundImg);
				Graphics2D graphic = img.createGraphics();
				
				graphic.setFont(new Font("Tahoma", Font.PLAIN, 11)); 
				
				int fpTotalCount = matches.size();
				int fpCount = 0;
				for (FingerprintScore match : matches) {
					if (fpCount > fpTotalCount - 10) {
						graphic.setColor(new Color(1.0f, 0.0f, 0.0f, (float) (match.getScore()/maxScore)));					
					}else{
						graphic.setColor(new Color(0.0f, 0.0f, 1.0f, (float) (match.getScore()/maxScore)));
					}
					fpCount++;
					graphic.drawString(String.format("%d", (int)(match.getScore()*10000)), match.getFingerprint().getX()-5, match.getFingerprint().getY()+5);
					//graphic.fillOval(match.getFingerprint().getX()-5, match.getFingerprint().getY()-5, 10, 10);
				}
				
				graphic.setColor(Color.black);
				graphic.drawString(String.format("%d %s Pos:%d Wifi %s", counter, timeStamp, positionNumber, label), 10, 10);
	
				ImageIO.write(img, "PNG", new File(Params.FILE_OUTPUT_DIR+"/"+counter+" "+label+".png"));
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	List<FingerprintScore> getTopXSeparatedFingerprints(int x, List<FingerprintScore> fingerprints) {
		List<FingerprintScore> returnList = new ArrayList<FingerprintScore>();
		
		if (x > fingerprints.size()) {
			x = fingerprints.size();
		}
		
		Collections.sort(fingerprints);
		// We return a list of the top X likely positions, in decreasing order of score
		// Higher score = more likely
		for (int i = (fingerprints.size() - 1); i >= 0; i--){
			FingerprintScore fp = fingerprints.get(i);
			boolean alreadyHaveNearby = false;
			for (FingerprintScore fp2 : returnList) {
				if (fp.getFingerprint().getPoint()
						.distance(fp2.getFingerprint().getPoint()) < 0.5 * Params.PIXELS_PER_METER) {
					alreadyHaveNearby = true;
				}
			}
			if (!alreadyHaveNearby) {
				returnList.add(fp);
			}
			if (returnList.size() >= x) {
				break;
			}
		}
		
		return returnList;		
	}

}
