package piloc.analysis3a;


public class Params {
	
	
	public static float PIXELS_PER_METER = 0;
	public static float MAP_ANGLE_OFFSET = 0;
	/*
	// COM1 B1
	public static float PIXELS_PER_METER = 21;
	public static float MAP_ANGLE_OFFSET = -57;
	*/
	/*
	// COM1 L1
	public static float PIXELS_PER_METER = 12;
	public static float MAP_ANGLE_OFFSET = -50.5f;
	*/
	/*
	// I3 L3
	public static float PIXELS_PER_METER = 17.5f;
	public static float MAP_ANGLE_OFFSET = -20;
	*/
	public static final boolean RENDER_IMAGES = false;
	
	public static final int LIMIT_WIFI_SCANS_TO_MOST_RECENT_MILLISECONDS = 200;
	//public static final Integer[] LIMIT_WIFI_SCAN_CHANNELS = {};
	public static final Integer[] LIMIT_WIFI_SCAN_CHANNELS = {2412, 2417, 2422, 2427, 2432, 2437, 2442, 2447, 2452, 2457, 2462, 2467, 2472, 2484};
	// public static final Integer[] LIMIT_WIFI_SCAN_CHANNELS = {4915, 4920, 4925, 4935, 4940, 4945, 4960, 4980, 5035, 5040, 5045, 5055, 5060, 5080, 5170, 5180, 5190, 5200, 5210, 5220, 5230, 5240, 5260, 5280, 5300, 5320, 5500, 5520, 5540, 5560, 5580, 5600, 5620, 5640, 5660, 5680, 5700, 5745, 5765, 5785, 5805, 5825};
	//public static final String[] LIMIT_WIFI_SCAN_SSID = {};
	public static final String[] LIMIT_WIFI_SCAN_SSID = {"NUS"};
	
	public static float getInitialStepLength(){
		return 0.6f * PIXELS_PER_METER;
	}
	
	public static final double STEP_COMPONENT_WEIGHT = 0.1;
	public static final double MOTION_STEP_LENGTH_WEIGHT = 0.1;
	public static float MAX_STEP_LENGTH = 1;
	
	public static String FILE_RADIO_MAP = "";
	public static String FILE_POSITION_MAP = "";
	public static String FILE_FLOOR_PLAN_IMG = "";
	public static String FILE_BACKGROUND_IMG = "";
	public static String FILE_FLOOR_PLAN_MASK_IMG = "";
	public static String FILE_INPUT_LOG = "";
	public static String FILE_OUTPUT_DIR = "";
	
	/*
	public static final String FILE_RADIO_MAP = "C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 11 Mar 2015/com1 b1/radiomap/radiomap.txt";//"gen radio map.txt"; //"com1 basement radio map.txt";
	public static final String FILE_POSITION_MAP = "com1b1\\posmap.txt";
	public static final String FILE_FLOOR_PLAN_IMG = "com1b1\\com1b1.png";
	public static final String FILE_BACKGROUND_IMG = "com1b1\\com1b1numbered.png";
	public static final String FILE_FLOOR_PLAN_MASK_IMG = "com1b1\\com1b1mask.png";
	public static String FILE_INPUT_LOG = "C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 11 Mar 2015/com1 b1/com1b1 slow wifi and stop - steptrace 20150311193337.txt";
	public static String FILE_OUTPUT_DIR = "output";
	*/
	/*
	public static final String FILE_RADIO_MAP = "C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 18 Mar 2015/com1l1/radiomap/radiomap.txt";
	public static final String FILE_POSITION_MAP = "com1l1/posmap.txt";
	public static final String FILE_FLOOR_PLAN_IMG = "com1l1/com1l1path.png";
	public static final String FILE_BACKGROUND_IMG = "com1l1/COM1_L1.jpg";
	public static final String FILE_FLOOR_PLAN_MASK_IMG = "com1l1/com1l1mask.png";
	public static String FILE_INPUT_LOG = "C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 18 Mar 2015/com1l1/wifi slow/steptrace 20150318190829.txt";
	public static String FILE_OUTPUT_DIR = "output";
	*/
	/*
	public static final String FILE_RADIO_MAP = "C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 11 Mar 2015/i3 lvl3/radiomap/radiomap.txt";
	public static final String FILE_POSITION_MAP = "i3/icubeL3posmap.txt";
	public static final String FILE_FLOOR_PLAN_IMG = "i3/icubeL3.png";
	public static final String FILE_BACKGROUND_IMG = "i3/icubeL3background.png";
	public static final String FILE_FLOOR_PLAN_MASK_IMG = "i3/icubel3mask.png";
	public static String FILE_INPUT_LOG = "C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 11 Mar 2015/i3 lvl3/i3 lvl3 clockwise wifi slow stop - steptrace 20150311203149.txt";
	public static String FILE_OUTPUT_DIR = "output";
	*/
	

}
