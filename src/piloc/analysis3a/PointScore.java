package piloc.analysis3a;

import java.awt.Point;

public class PointScore {
	
	private Point point;
	private double score;
	
	public PointScore(int x, int y, double score) {
		super();
		this.point = new Point(x, y);
		this.score = score;
	}
	
	public PointScore(Point point, double score) {
		super();
		this.point = point;
		this.score = score;
	}

	public Point getPoint() {
		return point;
	}
	
	public float getX(){
		return point.x;
	}
	
	public float getY(){
		return point.y;
	}
	
	public void setPoint(Point point) {
		this.point = point;
	}
	
	public double getScore() {
		return score;
	}
	
	public void setScore(float score) {
		this.score = score;
	}
	
	

}
