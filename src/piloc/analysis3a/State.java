package piloc.analysis3a;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import piloc.common.FingerprintScore;

public class State {

	public List<FingerprintScore> fingerprints;
	public List<Step> transitionSteps;
	
	public State(List<FingerprintScore> fingerprints) {
		super();
		this.fingerprints = fingerprints;
		transitionSteps = new LinkedList<Step>();
	}

	public void addFingerprintScore(FingerprintScore fp) {
		fingerprints.add(fp);
	}
	
	public List<FingerprintScore> getTopXFingerprints(int x) {
		List<FingerprintScore> returnList = new ArrayList<FingerprintScore>();
		
		if (x > fingerprints.size()) {
			x = fingerprints.size();
		}
		
		Collections.sort(fingerprints);
		int counter = fingerprints.size() - 1;
		for (int i = 0; i < x; i++) {
			returnList.add(fingerprints.get(counter));
			counter--;
		}
		
		return returnList;
		
	}
	
	public List<FingerprintScore> getFingerprintsInDecendingProbability() {
		return getTopXFingerprints(fingerprints.size());		
	}
	
	public void addStep(Step step){
		transitionSteps.add(step);
	}
	
	public List<Step> getSteps() {
		return transitionSteps;
	}
	
}
