package piloc.analysisWifi;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import piloc.common.ArrayStats;

public class BatchRun {
	
	public static String SOURCE_FOLDER = "";
	public static String OUTPUT_FOLDER = "";
	
	public static String GRAPH_TITLE = "";
	public static String GRAPH_TITLE_IGNORE = "";
	
	public static final int IGNORE_STARTING_N_LINES = 5;

	public static final Integer[] CHANNELS_ALL = {};
	public static final Integer[] CHANNELS_5GHz = { 4915, 4920, 4925, 4935, 4940,
			4945, 4960, 4980, 5035, 5040, 5045, 5055, 5060, 5080, 5170, 5180,
			5190, 5200, 5210, 5220, 5230, 5240, 5260, 5280, 5300, 5320, 5500,
			5520, 5540, 5560, 5580, 5600, 5620, 5640, 5660, 5680, 5700, 5745,
			5765, 5785, 5805, 5825 };
	public static final Integer[] CHANNELS_2_4GHz = { 2412, 2417, 2422, 2427, 2432,
			2437, 2442, 2447, 2452, 2457, 2462, 2467, 2472, 2484 };

	public static final String[] SSID_ALL = {};
	public static final String[] SSID_NUS = { "NUS" };

	public static void main(String args[]) throws IOException {

		//configureForCOM1B1();
		//process();
		//configureForCOM1L1();
		//process();
		//configureForI3L3();
		//process();
		configureForCreateL2();
		process();
	}

	public static void process() throws IOException, FileNotFoundException {
		
		File f = new File(SOURCE_FOLDER);
		File[] inputTraceFiles = f.listFiles();

		String mode = "";
		makeFolderIfNotExists(OUTPUT_FOLDER);
		
		BufferedWriter w = new BufferedWriter(new FileWriter(
				OUTPUT_FOLDER + "/errorsummary.txt"));
		
		for (int i = 0; i < 6; i++) {
			switch (i) {
			case 0:
				mode = "All WiFi";
				Params.LIMIT_WIFI_SCAN_SSID = SSID_ALL;
				Params.LIMIT_WIFI_SCAN_CHANNELS = CHANNELS_ALL;
				break;
			case 1:
				mode = "All NUS";
				Params.LIMIT_WIFI_SCAN_SSID = SSID_NUS;
				Params.LIMIT_WIFI_SCAN_CHANNELS = CHANNELS_ALL;
				break;
			case 2:
				mode = "5GHz only";
				Params.LIMIT_WIFI_SCAN_SSID = SSID_ALL;
				Params.LIMIT_WIFI_SCAN_CHANNELS = CHANNELS_5GHz;
				break;
			case 3:
				mode = "5GHz + NUS";
				Params.LIMIT_WIFI_SCAN_SSID = SSID_NUS;
				Params.LIMIT_WIFI_SCAN_CHANNELS = CHANNELS_5GHz;
				break;
			case 4:
				mode = "2.4GHz only";
				Params.LIMIT_WIFI_SCAN_SSID = SSID_ALL;
				Params.LIMIT_WIFI_SCAN_CHANNELS = CHANNELS_2_4GHz;
				break;
			case 5:
				mode = "2.4GHz + NUS";
				Params.LIMIT_WIFI_SCAN_SSID = SSID_NUS;
				Params.LIMIT_WIFI_SCAN_CHANNELS = CHANNELS_2_4GHz;
				break;
			default:
				mode = "Default";
				Params.LIMIT_WIFI_SCAN_SSID = SSID_ALL;
				Params.LIMIT_WIFI_SCAN_CHANNELS = CHANNELS_ALL;
				break;
			}
			
			String outputFolderWithParams = OUTPUT_FOLDER + "/" + mode;
			makeFolderIfNotExists(outputFolderWithParams);

			List<Double> errorValues = new LinkedList<Double>();
		
			for (int j = 0; j < inputTraceFiles.length; j++) {	
				File traceFile = inputTraceFiles[j];
				Params.FILE_INPUT_LOG = traceFile.getAbsolutePath();
				String traceFileName = traceFile.getName().split("\\.")[0];
				Params.FILE_OUTPUT_DIR = outputFolderWithParams + "/"
						+ traceFileName;
				System.out.println(Params.FILE_OUTPUT_DIR);
				makeFolderIfNotExists(Params.FILE_OUTPUT_DIR);
	
				// Run algorithm
				try {
					Main.process();
				} catch (Exception e) {
					e.printStackTrace();
				}
	
				// Read errorstats and populate histogramBins
	
				BufferedReader r = new BufferedReader(new FileReader(
						Params.FILE_OUTPUT_DIR + "/errorstats.txt"));
				String line;
	
				while ((line = r.readLine()) != null) {
	
					String[] lineParts = line.split("\t");
	
					double wifiError = Double.parseDouble(lineParts[3]);
					errorValues.add(wifiError);
	
				}
	
				r.close();
			}
			
			logAndDisplay(w, "Mode: " + mode);			
			logAndDisplay(w, String.format(
					"Avg:\t%f%nMedian:\t%f%nAvg (m)\t%f%nMedian (m):\t%f%n75th Percentile:\t%f%n90th Percentile:\t%f",
					ArrayStats.getAverage(errorValues), 
					ArrayStats.getMedian(errorValues), 
					ArrayStats.getAverage(errorValues) / Params.PIXELS_PER_METER, 
					ArrayStats.getMedian(errorValues) / Params.PIXELS_PER_METER,
					ArrayStats.getPercentile(75, errorValues) / Params.PIXELS_PER_METER, 
					ArrayStats.getPercentile(90, errorValues) / Params.PIXELS_PER_METER)
					);
			logAndDisplay(w, ArrayStats.getHistogramBucketString(errorValues, Params.PIXELS_PER_METER));
			logAndDisplay(w, System.lineSeparator());
			
		}
		
		w.close();
	}
	
	public static void logAndDisplay(BufferedWriter w, String s) {
		try {
			w.write(s);
			w.newLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(s);
	}

	public static void makeFolderIfNotExists(String folder)
			throws IOException {
		File outputFolder = new File(folder);
		if (!outputFolder.exists()){
			Files.createDirectories(outputFolder.toPath());
		}
	}
	
	private static String getOutputFolderParams() {
		String date = "1Apr";
		
		return String.format(" %s", date);
		
	}
	
	public static void configureForCOM1B1(){
		
		SOURCE_FOLDER = "C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 11 Mar 2015/com1 b1/wifi slow";
		OUTPUT_FOLDER = "batchoutput algowifi com1b1";
		OUTPUT_FOLDER += getOutputFolderParams();
		
		Params.FILE_RADIO_MAP = "C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 11 Mar 2015/com1 b1/radiomap/radiomap.txt";//"gen radio map.txt"; //"com1 basement radio map.txt";
		Params.FILE_POSITION_MAP = "com1b1/posmap.txt";
		Params.FILE_FLOOR_PLAN_IMG = "com1b1/com1b1.png";
		Params.FILE_BACKGROUND_IMG = "com1b1/com1b1numbered.png";
		Params.FILE_FLOOR_PLAN_MASK_IMG = "com1b1/com1b1mask.png";
		
		Params.PIXELS_PER_METER = 16;
		Params.MAP_ANGLE_OFFSET = -51;
		
		GRAPH_TITLE = "COM1B1 (slow WiFi) history=%d";
		GRAPH_TITLE_IGNORE = "COM1B1 (slow WiFi) history=%d ignoring first %d";
	}

	public static void configureForCOM1L1(){
		
		SOURCE_FOLDER = "C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 18 Mar 2015/com1l1/wifi slow";
		OUTPUT_FOLDER = "batchoutput algowifi com1l1";
		OUTPUT_FOLDER += getOutputFolderParams();
		
		Params.FILE_RADIO_MAP = "C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 18 Mar 2015/com1l1/radiomap/radiomap.txt";//"gen radio map.txt"; //"com1 basement radio map.txt";
		Params.FILE_POSITION_MAP = "com1l1/posmap.txt";
		Params.FILE_FLOOR_PLAN_IMG = "com1l1/com1l1path.png";
		Params.FILE_BACKGROUND_IMG = "com1l1/COM1_L1.jpg";
		Params.FILE_FLOOR_PLAN_MASK_IMG = "com1l1/com1l1mask.png";
		
		Params.PIXELS_PER_METER = 8.2f;
		Params.MAP_ANGLE_OFFSET = -50;
		
		GRAPH_TITLE = "COM1L1 (slow WiFi) history=%d";
		GRAPH_TITLE_IGNORE = "COM1L1 (slow WiFi) history=%d ignoring first %d";
	}
	
	public static void configureForI3L3(){
		
		SOURCE_FOLDER = "C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 11 Mar 2015/i3 lvl3/wifi slow";
		OUTPUT_FOLDER = "batchoutput algowifi i3lvl3";
		OUTPUT_FOLDER += getOutputFolderParams();
		
		Params.FILE_RADIO_MAP = "C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 11 Mar 2015/i3 lvl3/radiomap/radiomap.txt";//"gen radio map.txt"; //"com1 basement radio map.txt";
		Params.FILE_POSITION_MAP = "i3/posmap.txt";
		Params.FILE_FLOOR_PLAN_IMG = "i3/icubeL3.png";
		Params.FILE_BACKGROUND_IMG = "i3/icubeL3background.png";
		Params.FILE_FLOOR_PLAN_MASK_IMG = "i3/icubel3mask.png";
		
		Params.PIXELS_PER_METER = 8.6f;
		Params.MAP_ANGLE_OFFSET = -20;
		
		GRAPH_TITLE = "I3L3 (slow WiFi) history=%d";
		GRAPH_TITLE_IGNORE = "I3L3 (slow WiFi) history=%d ignoring first %d";
	}
	
	public static void configureForCreateL2(){
		
		SOURCE_FOLDER = "C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 20 Mar 2015/createl2/wifi slow";
		OUTPUT_FOLDER = "batchoutput algowifi createl2";
		OUTPUT_FOLDER += getOutputFolderParams();
		
		Params.FILE_RADIO_MAP = "C:/Users/Kai Yao/OneDrive/Documents/FYP/Data 20 Mar 2015/createl2/radiomap/radiomap.txt";//"gen radio map.txt"; //"com1 basement radio map.txt";
		Params.FILE_POSITION_MAP = "createl2/posmap.txt";
		Params.FILE_FLOOR_PLAN_IMG = "createl2/path.png";
		Params.FILE_BACKGROUND_IMG = "createl2/background.png";
		Params.FILE_FLOOR_PLAN_MASK_IMG = "createl2/mask.png";
		
		Params.PIXELS_PER_METER = 13.25f;
		Params.MAP_ANGLE_OFFSET = 23.36f;
		
		GRAPH_TITLE = "CreateL2 (slow WiFi) history=%d";
		GRAPH_TITLE_IGNORE = "CreateL2 (slow WiFi) history=%d ignoring first %d";
	}
	
}
