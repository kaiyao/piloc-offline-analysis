package piloc.analysisWifi;


public class Params {
	
	
	public static float PIXELS_PER_METER = 0;
	public static float MAP_ANGLE_OFFSET = 0;
	
	public static final int LIMIT_WIFI_SCANS_TO_MOST_RECENT_MILLISECONDS = 200;
	public static Integer[] LIMIT_WIFI_SCAN_CHANNELS = {};
	//public static final Integer[] LIMIT_WIFI_SCAN_CHANNELS = {2412, 2417, 2422, 2427, 2432, 2437, 2442, 2447, 2452, 2457, 2462, 2467, 2472, 2484};
	// public static final Integer[] LIMIT_WIFI_SCAN_CHANNELS = {4915, 4920, 4925, 4935, 4940, 4945, 4960, 4980, 5035, 5040, 5045, 5055, 5060, 5080, 5170, 5180, 5190, 5200, 5210, 5220, 5230, 5240, 5260, 5280, 5300, 5320, 5500, 5520, 5540, 5560, 5580, 5600, 5620, 5640, 5660, 5680, 5700, 5745, 5765, 5785, 5805, 5825};
	public static String[] LIMIT_WIFI_SCAN_SSID = {};
	//public static final String[] LIMIT_WIFI_SCAN_SSID = {"NUS"};
	
	public static float getInitialStepLength(){
		return 0.6f * PIXELS_PER_METER;
	}
	
	public static final double STEP_COMPONENT_WEIGHT = 0.1;
	public static final double MOTION_STEP_LENGTH_WEIGHT = 0.1;
	
	public static String FILE_RADIO_MAP = "";
	public static String FILE_POSITION_MAP = "";
	public static String FILE_FLOOR_PLAN_IMG = "";
	public static String FILE_BACKGROUND_IMG = "";
	public static String FILE_FLOOR_PLAN_MASK_IMG = "";
	public static String FILE_INPUT_LOG = "";
	public static String FILE_OUTPUT_DIR = "";
	

}
