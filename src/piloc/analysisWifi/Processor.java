package piloc.analysisWifi;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.imageio.ImageIO;

import piloc.common.Fingerprint;
import piloc.common.FingerprintScore;
import piloc.common.PositionIdMap;
import piloc.common.RadioMap;

public class Processor {
	
	RadioMap radioMap;
	
	int counter = 0;
	int positionNumber = 0;
	Point lastWifiEstimatedPosition = null;
	private BufferedWriter errorStatsFileWriter;
	
	PositionIdMap positionIdMap;

	private BufferedWriter particlesFileWriter;
	
	public Processor() {
		radioMap = new RadioMap(Params.FILE_RADIO_MAP);
		
		try {
			errorStatsFileWriter = new BufferedWriter(new FileWriter(Params.FILE_OUTPUT_DIR+"/"+"errorstats.txt"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		positionIdMap = new PositionIdMap(Params.FILE_POSITION_MAP);
	}
	
	public void onWifiScanReceived(String timeStamp, Fingerprint fp){
		
		List<FingerprintScore> matches = radioMap.getWeightedFpMatchByScore(fp);
					
		if (lastWifiEstimatedPosition == null) {
			Fingerprint mostLikelyFingerprint = matches.get(matches.size()-1).getFingerprint();
			lastWifiEstimatedPosition = new Point(mostLikelyFingerprint.getX(), mostLikelyFingerprint.getY());
			recordPositionIfDataAvailable();
			recordNumberOfMatchesRequiredForAccuracy(matches);
		}
	}

	public void onStepOccurred(String timeStamp) {		
	}
	
	public void onCompassDataReceived(String timeStamp, float mCurrentAngle) {		
	}

	public void onPositionRecordOccurred(String timeStamp, int positionNumber) {
		
		// TODO Auto-generated method stub
		this.positionNumber = positionNumber;
		resetRecordPositionData();
	}
	
	public void resetRecordPositionData(){
		lastWifiEstimatedPosition = null;
	}
	
	public void recordPositionIfDataAvailable(){
		if (positionIdMap.getLocationOfId(positionNumber) == null || this.lastWifiEstimatedPosition == null) {
			return;
		}
		
		Point actualPosCoords = positionIdMap.getLocationOfId(positionNumber);
		Point wifiPosCoords = lastWifiEstimatedPosition;
		double wifiDistanceError = actualPosCoords.distance(wifiPosCoords);
		
		try {
			errorStatsFileWriter.write(String.format("%d\t%d,%d\t%d,%d\t%f", 
					positionNumber,	
					actualPosCoords.x, actualPosCoords.y,
					wifiPosCoords.x, wifiPosCoords.y,
					wifiDistanceError
					));
			errorStatsFileWriter.newLine();
			errorStatsFileWriter.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	int[] matchesRequiredFor1m = new int[11];
	int[] matchesRequiredFor2m = new int[11];

	private void recordNumberOfMatchesRequiredForAccuracy(
			List<FingerprintScore> matches) {
		if (positionIdMap.getLocationOfId(positionNumber) == null){
			return;
		}
		
		Point actualPosCoords = positionIdMap.getLocationOfId(positionNumber);
		
		int pointNumber = 0;
		
		Collections.reverse(matches);
		
		for (FingerprintScore fps : matches) {
			if (fps.getFingerprint().getPoint().distance(actualPosCoords) <= 1.0 * Params.PIXELS_PER_METER) {
				int pNumber = pointNumber;
				if (pNumber > 10){
					pNumber = 10;
				}	
				matchesRequiredFor1m[pNumber]++;
				break;
			}
			pointNumber++;
		}
		
		pointNumber = 0;
		
		for (FingerprintScore fps : matches) {
			if (fps.getFingerprint().getPoint().distance(actualPosCoords) <= 2.0 * Params.PIXELS_PER_METER) {
				int pNumber = pointNumber;
				if (pNumber > 10){
					pNumber = 10;
				}	
				matchesRequiredFor2m[pNumber]++;
				break;
			}
			pointNumber++;
		}
	}
	
	public <T> String arrayToTabDelimitedString(T[] array){
		StringBuffer tabbedString = new StringBuffer();
		for (int i = 0; i < array.length; i++) {
			tabbedString.append(array[i] + "\t");				
		}
		return tabbedString.toString();
	}
	
	public String arrayToTabDelimitedString(int[] array){
		StringBuffer tabbedString = new StringBuffer();
		for (int i = 0; i < array.length; i++) {
			tabbedString.append(array[i] + "\t");				
		}
		return tabbedString.toString();	
	}

	public void finished() {
		// TODO Auto-generated method stub
		System.out.println(arrayToTabDelimitedString(matchesRequiredFor1m));
		System.out.println(arrayToTabDelimitedString(matchesRequiredFor2m));
	}

}
