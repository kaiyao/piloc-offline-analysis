package piloc.analysis2;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.imageio.ImageIO;

import piloc.common.Fingerprint;
import piloc.common.FingerprintScore;
import piloc.common.PositionIdMap;
import piloc.common.RadioMap;

public class Processor {
	
	RadioMap radioMap;
	
	int counter = 0;
	float currentAngle = 0;
	int positionNumber = 0;
	Point lastEstimatedPosition = null;
	Point lastWifiEstimatedPosition = null;
	private BufferedWriter errorStatsFileWriter;
	
	PositionIdMap positionIdMap;
	
	private BufferedImage backgroundImg;
	private BufferedImage floorPlanImg;
	private BufferedImage maskImg;
	
	private List<State> states;
	private State currentState;
	
	float stepLength = Params.getInitialStepLength();

	private BufferedWriter particlesFileWriter;
	
	public Processor() {
		radioMap = new RadioMap(Params.FILE_RADIO_MAP);
		
		try {
			errorStatsFileWriter = new BufferedWriter(new FileWriter(Params.FILE_OUTPUT_DIR+"/"+"errorstats.txt"));
			particlesFileWriter = new BufferedWriter(new FileWriter(Params.FILE_OUTPUT_DIR+"/"+"stepLength.txt"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		positionIdMap = new PositionIdMap(Params.FILE_POSITION_MAP);
		
		try {
			backgroundImg = ImageIO.read(new File(Params.FILE_BACKGROUND_IMG));
			floorPlanImg = ImageIO.read(new File(Params.FILE_FLOOR_PLAN_IMG));
			maskImg = ImageIO.read(new File(Params.FILE_FLOOR_PLAN_MASK_IMG));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		states = new ArrayList<State>();
	}
	
	public void onWifiScanReceived(String timeStamp, Fingerprint fp){
		
		List<FingerprintScore> matches = radioMap.getWeightedFpMatchByScore(fp);
		
		// Ignore multiple wifi at same position
		if (currentState != null && currentState.getSteps().size() == 0){
			return;
		}
		
		currentState = new State(matches);
		states.add(currentState);
		
		processChain(timeStamp);
		
		renderWifiImage(timeStamp, matches);
		
		System.out.println(counter);
		counter++;
					
		if (lastWifiEstimatedPosition == null) {
			Fingerprint mostLikelyFingerprint = matches.get(matches.size()-1).getFingerprint();
			lastWifiEstimatedPosition = new Point(mostLikelyFingerprint.getX(), mostLikelyFingerprint.getY());
			recordPositionIfDataAvailable();
		}
	}

	private void renderWifiImage(String timeStamp,
			List<FingerprintScore> matches) {
		if (Params.RENDER_IMAGES) {
			double maxScore = 0;
			for (FingerprintScore match : matches) {
				if (match.getScore() > maxScore) {
					maxScore = match.getScore();
				}
			}
			try {
				BufferedImage img = copyImage(backgroundImg);
				Graphics2D graphic = img.createGraphics();
				
				graphic.setFont(new Font("Tahoma", Font.PLAIN, 11)); 
				
				int fpTotalCount = matches.size();
				int fpCount = 0;
				for (FingerprintScore match : matches) {
					if (fpCount > fpTotalCount - 10) {
						graphic.setColor(new Color(1.0f, 0.0f, 0.0f, (float) (match.getScore()/maxScore)));					
					}else{
						graphic.setColor(new Color(0.0f, 0.0f, 1.0f, (float) (match.getScore()/maxScore)));
					}
					fpCount++;
					graphic.drawString(String.format("%d", (int)(match.getScore()*10000)), match.getFingerprint().getX()-5, match.getFingerprint().getY()+5);
					//graphic.fillOval(match.getFingerprint().getX()-5, match.getFingerprint().getY()-5, 10, 10);
				}
				
				graphic.setColor(Color.black);
				graphic.drawString(String.format("%d %s Pos:%d Wifi", counter, timeStamp, positionNumber), 10, 10);
	
				ImageIO.write(img, "PNG", new File(Params.FILE_OUTPUT_DIR+"/"+counter+" wifi.png"));
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	private void processChain(String timeStamp) {
		
		int historyLength = Params.HISTORY_LENGTH;
		
		if (historyLength > states.size()) historyLength = states.size();
		
		List<BeliefParticle> particles = null;
		
		Point mostLikelyStartPoint = new Point(0,0);
		Point mostLikelyEndingPoint = new Point(0,0);
		BeliefParticle uncompensatedMotion = new BeliefParticle(0,0);
		
		BufferedImage img;
		Graphics2D graphic;
		
		if (Params.RENDER_IMAGES) {
			img = copyImage(backgroundImg);
			graphic = img.createGraphics();
			graphic.setFont(new Font("Tahoma", Font.PLAIN, 11)); 
			
			int startState = states.size() - historyLength + 1;
			if (startState >= states.size() ) startState = states.size() - 1;
			int endState = states.size() - 1;
			
			List<FingerprintScore> fingerprintScores = states.get(startState)
					.getTopXSeparatedFingerprints(Params.WIFI_TOP);
			for (FingerprintScore fps : fingerprintScores) {
				int x = fps.getFingerprint().getX();
				int y = fps.getFingerprint().getY();
				
				graphic.setColor(new Color(0.0f, 0.0f, 1.0f, 0.5f));				
				graphic.fillOval((int)(x-5), (int)(y-5), 10, 10);
			}
			
			graphic.setColor(Color.black);
			graphic.drawString(String.format("%d %s StartState:%d EndState:%d", counter, timeStamp, startState, endState), 10, 10);
			
			try {
				ImageIO.write(img, "PNG", new File(Params.FILE_OUTPUT_DIR+"/"+counter+"-start.png"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		boolean isFirstState = true;
		for (int i = states.size() - historyLength; i < states.size(); i++) {			
			
			if (Params.RENDER_IMAGES) {
				img = copyImage(backgroundImg);
				graphic = img.createGraphics();
				graphic.setFont(new Font("Tahoma", Font.PLAIN, 11)); 
			}
			
			State state = states.get(i);
			State prevState = null;
			if (i > states.size() - historyLength) {
				prevState = states.get(i-1);
			}
			
			List<FingerprintScore> fingerprintScores = state.getTopXSeparatedFingerprints(Params.WIFI_TOP);
			
			if (prevState == null) {
				
				// Normalize scores of particles
				double sumScore = 0;
				for (FingerprintScore fps : fingerprintScores) {
					sumScore += fps.getScore();
				}
				
				// Assign belief to wifi points
				particles = new ArrayList<BeliefParticle>();
				for (FingerprintScore fps : fingerprintScores) {
					BeliefParticle particle = new BeliefParticle(
							fps.getFingerprint().getX(),
							fps.getFingerprint().getY(),
							fps.getScore()/sumScore);
					particles.add(particle);
				}
			
			}else{
				
				// For each step, apply to the belief particles
				int stepCount = 0;
				for (Step step : prevState.getSteps()) {
					for (BeliefParticle particle : particles) {
						particle.moveStep(step.getDistance(), step.getAngle());
						
						if (Params.RENDER_IMAGES) {
							graphic.setColor(new Color(0.0f, 0.0f, 1.0f, 0.2f));				
							graphic.fillOval((int)(particle.getX()-5), (int)(particle.getY()-5), 10, 10);
						}
					}
					logParticles(particles, stepLength, timeStamp);
				}
				
				// Assign belief to wifi points
				// Set weight based on how far wifi point is from particles
				ArrayList<BeliefParticle> newParticles = new ArrayList<BeliefParticle>();
				
				double sumScore = 0;
				for (FingerprintScore fps : fingerprintScores) {
					sumScore += fps.getScore();
				}
				
				for (FingerprintScore fps : fingerprintScores) {
					// For each wifi point in new scan
					
					// We calculate the effective "score" at these new wifi points
					
					// We use the previous wifi points' scores in conjunction
					// with the distance of those points to the new points
					// (after compensating the old points with motion as above)
					Point fpPoint = new Point(fps.getFingerprint().getX(), fps.getFingerprint().getY());
					double scoreFromPrevState = 0;
					for (BeliefParticle p : particles){
						Point pPoint = p.getPoint();
						double distance = fpPoint.distance(pPoint);
						
						// smaller distance = better
						// larger weight = better
						scoreFromPrevState += 1/(distance+1) * p.getWeight();
					}
					
					BeliefParticle particle = new BeliefParticle(
						fps.getFingerprint().getX(),
						fps.getFingerprint().getY(),
						(fps.getScore()/sumScore) * scoreFromPrevState);
					newParticles.add(particle);
				}
				
				// Draw new wifi scan locations
				for (BeliefParticle particle : newParticles) {
					
					if (Params.RENDER_IMAGES) {
						graphic.setColor(new Color(0.0f, 1.0f, 0.0f, 0.5f));				
						graphic.fillOval((int)(particle.getX()-5), (int)(particle.getY()-5), 10, 10);
					}
				}
				
				particles = newParticles;
				
			}
			
			// Log the most likely position
			BeliefParticle mostLikelyParticle = particles.get(0);
			for(BeliefParticle particle : particles) {
				if (particle.getWeight() > mostLikelyParticle.getWeight()){
					mostLikelyParticle = particle;
				}
			}
			lastEstimatedPosition = mostLikelyParticle.getPoint();			
			
			
			
			if (Params.RENDER_IMAGES) {
				try {
					
					// Draw location estimate circle
					//graphic.setColor(new Color(0.0f, 1.0f, 0.0f, 1.0f));			
					//graphic.fillOval((int)(lastEstimatedPosition.getX()-5), (int)(lastEstimatedPosition.getY()-5), 10, 10);
					
					// Write text header
					graphic.setColor(Color.black);
					graphic.drawString(String.format("%d %s State:%d Pos:%d", counter, timeStamp, i, positionNumber), 10, 10);
					
					ImageIO.write(img, "PNG", new File(Params.FILE_OUTPUT_DIR+"/"+counter+"-"+i+".png"));
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		// Adjust step length
		double uncompensatedMotionDistance = uncompensatedMotion.getPoint().distance(0, 0);
		if (uncompensatedMotionDistance > 0) {
			double expectedDisplacement = mostLikelyStartPoint.distance(lastEstimatedPosition);
			
			double scalingFactor = expectedDisplacement / uncompensatedMotionDistance;
			
			double newStepLength = scalingFactor * stepLength;
			// Ignore new step length if it is too much... something must be wrong
			if (newStepLength < 1.2 * Params.PIXELS_PER_METER) {
				stepLength = (float) (stepLength * (1-Params.MOTION_STEP_LENGTH_WEIGHT) + newStepLength * Params.MOTION_STEP_LENGTH_WEIGHT);
			}
		}
		//renderImage(particles, timeStamp);
		
	}

	private void renderImage(List<BeliefParticle> particles, String timeStamp) {
		if (Params.RENDER_IMAGES) {
			
			double maxWeight = 0;
			for (BeliefParticle p : particles) {
				if (p.getWeight() > maxWeight){
					maxWeight = p.getWeight();
				}
			}
			
			try {
				BufferedImage img = copyImage(backgroundImg);
				Graphics2D graphic = img.createGraphics();
				
				graphic.setFont(new Font("Tahoma", Font.PLAIN, 11)); 
				
				for (BeliefParticle p : particles) {
					graphic.setColor(new Color(0.0f, 0.0f, 1.0f, (float) (p.getWeight()/maxWeight)));
					graphic.fillOval((int)(p.getX()-5), (int)(p.getY()-5), 10, 10);
				}
				
				graphic.setColor(new Color(0.0f, 1.0f, 0.0f, 1.0f));				
				graphic.fillOval((int)(lastEstimatedPosition.getX()-5), (int)(lastEstimatedPosition.getY()-5), 10, 10);
				
				graphic.setColor(Color.black);
				graphic.drawString(String.format("%d %s Pos:%d Step", counter, timeStamp, positionNumber), 10, 10);
				
				ImageIO.write(img, "PNG", new File(Params.FILE_OUTPUT_DIR+"/"+counter+".png"));
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}

	public void onStepOccurred(String timeStamp) {
		if (currentState == null) {
			return;
		}
		
		float stepAngle = (currentAngle + 360 + Params.MAP_ANGLE_OFFSET) % 360;
		
		currentState.addStep(new Step(stepLength, stepAngle));
		
		System.out.println(counter);
		counter++;
	}
	
	public void onCompassDataReceived(String timeStamp, float mCurrentAngle) {
		currentAngle = mCurrentAngle;
	}

	public void onPositionRecordOccurred(String timeStamp, int positionNumber) {
		
		// TODO Auto-generated method stub
		this.positionNumber = positionNumber;
		resetRecordPositionData();
	}
	
	public void resetRecordPositionData(){
		lastEstimatedPosition = null;
		lastWifiEstimatedPosition = null;
	}
	
	public void recordPositionIfDataAvailable(){
		if (positionIdMap.getLocationOfId(positionNumber) == null || this.lastWifiEstimatedPosition == null) {
			return;
		}
		
		if (this.lastEstimatedPosition == null) {
			this.lastEstimatedPosition = new Point(0,0);
		}
		
		Point actualPosCoords = positionIdMap.getLocationOfId(positionNumber);
		Point estimatedPosCoords = lastEstimatedPosition;
		double estDistanceError = actualPosCoords.distance(estimatedPosCoords);
		Point wifiPosCoords = lastWifiEstimatedPosition;
		double wifiDistanceError = actualPosCoords.distance(wifiPosCoords);
		
		try {
			errorStatsFileWriter.write(String.format("%d\t%d,%d\t%d,%d\t%f\t%d,%d\t%f", 
					positionNumber,	
					actualPosCoords.x, actualPosCoords.y,
					wifiPosCoords.x, wifiPosCoords.y,
					wifiDistanceError,
					estimatedPosCoords.x, estimatedPosCoords.y,						
					estDistanceError));
			errorStatsFileWriter.newLine();
			errorStatsFileWriter.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	static BufferedImage copyImage(BufferedImage bi) {
		/*ColorModel cm = bi.getColorModel();
		boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
		WritableRaster raster = bi.copyData(null);
		return new BufferedImage(cm, raster, isAlphaPremultiplied, null);*/
		return new BufferedImage(bi.getWidth(), bi.getHeight(), BufferedImage.TYPE_INT_ARGB);
	}
	
	private float determineStepLength(Point start, Point end, Point expectedEnd, float stepLengthUsed) {
		
		double traceDisplacement = start.distance(end);
		double expectedDisplacement = start.distance(expectedEnd);
		
		double scalingFactor = expectedDisplacement / traceDisplacement;
		
		return (float) (stepLengthUsed * scalingFactor);
		
	}
	
	void logParticles(List<BeliefParticle> particles, float stepLength, String extra){
		try {
			particlesFileWriter.write(String.format("* %d Pos:%d %s", counter, positionNumber, extra));
			particlesFileWriter.newLine();
			for (BeliefParticle particle : particles) {
				particlesFileWriter.write(String.format("%f\t%f\t%f\t%f", particle.getX(), particle.getY(), particle.getWeight(), stepLength));
				particlesFileWriter.newLine();
			}
			particlesFileWriter.newLine();
			particlesFileWriter.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void finished() {
		// TODO Auto-generated method stub
		
	}

}
