package piloc.analysis2;

import java.awt.Point;

public class BeliefParticle {

	private float x;
	private float y;
	private double weight;
	
	public BeliefParticle(float x, float y, double weight) {
		super();
		this.x = x;
		this.y = y;
		this.weight = weight;
	}
	
	public BeliefParticle(float x, float y) {
		this(x, y, 0);
	}
	
	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}
	
	public Point getPoint(){
		return new Point((int)x, (int)y);
	}

	public double getWeight() {
		return weight;
	}

	public void moveStep(float distance, float bearing){
		
		double bearingInRadians = bearing * Math.PI / 180.0;
		
		float newX = (float) (x + distance * Math.sin(bearingInRadians));
		float newY = (float) (y - distance * Math.cos(bearingInRadians));
		
		this.x = newX;
		this.y = newY;
	}
	
}
