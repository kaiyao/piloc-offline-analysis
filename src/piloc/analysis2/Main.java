package piloc.analysis2;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.util.Arrays;

import piloc.common.Fingerprint;
import piloc.common.ReadWifiData;

public class Main {
	
	public static void main(String[] args) throws IOException{
		
		if (args.length >= 2) {
			Params.FILE_INPUT_LOG = args[0];
			Params.FILE_OUTPUT_DIR = args[1];
		}else{
		
			// Clean output folder
			File outputFolder = new File(Params.FILE_OUTPUT_DIR);
			if (!outputFolder.exists()){
				Files.createDirectories(outputFolder.toPath());
			}
			
			final File[] files = outputFolder.listFiles();
			for (File f: files) f.delete();
		
		}
		
		process();
	
	}

	public static void process() {
		Processor processor = new Processor();
	
		BufferedReader br;
		try {
			String line;
			br = new BufferedReader(new FileReader(Params.FILE_INPUT_LOG));
			while ((line = br.readLine()) != null) {
				String lineParts[] = line.split("\t");
				String timeStamp = lineParts[0];
				String type = lineParts[1];
				
				if (type.equals("COMPASS")){
					
					float azimuth = Float.parseFloat(lineParts[2]);
					float pitch = Float.parseFloat(lineParts[3]);
					float roll = Float.parseFloat(lineParts[4]);
					
					//mCurrentAngle = (int)((450-azimuth)%360);
			    	float mCurrentAngle = (float) Math.toDegrees(azimuth);
			    	
			    	processor.onCompassDataReceived(timeStamp, mCurrentAngle);
			    	
				}
				
				if (type.equals("ACCELEROMETER")){
					float x2 = Float.parseFloat(lineParts[2]);
					float y2 = Float.parseFloat(lineParts[3]);
					float z2 = Float.parseFloat(lineParts[4]);
					
				}
				
				if (type.equals("WIFI")){
					Fingerprint fp = new Fingerprint();					
					ReadWifiData
					.readWifiData(
							br,
							lineParts,
							fp,
							Params.LIMIT_WIFI_SCANS_TO_MOST_RECENT_MILLISECONDS,
							Params.LIMIT_WIFI_SCAN_CHANNELS,
							Params.LIMIT_WIFI_SCAN_SSID);				
					if (fp.getBssids().size() > 0) {
						processor.onWifiScanReceived(timeStamp, fp);
					}
				}
				
				if (type.equals("STEP")){
					
					processor.onStepOccurred(timeStamp);
					
				}
				
				if (type.equals("POSITIONID")){
					
					int positionNumber = Integer.parseInt(lineParts[2]);
					processor.onPositionRecordOccurred(timeStamp, positionNumber);
					
				}
				
			}
			
			processor.finished();
			logParams();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static void logParams() throws IOException {
		try {
			BufferedWriter paramsWriter = new BufferedWriter(new FileWriter(Params.FILE_OUTPUT_DIR+"/"+"params.txt"));
			for (Field field : Params.class.getDeclaredFields()) {
			    field.setAccessible(true);
			    String name = field.getName();
			    Object value = field.get(Params.class);
			    if (value instanceof Object[]) {
			    	String arrayStr = Arrays.deepToString((Object[]) value);
			    	paramsWriter.write(String.format("%s: %s%n", name, arrayStr));
			    }else{
			    	paramsWriter.write(String.format("%s: %s%n", name, value));
			    }
			}
			paramsWriter.close();
		} catch (SecurityException | IllegalArgumentException
				| IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
