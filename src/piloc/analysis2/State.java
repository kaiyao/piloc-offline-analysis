package piloc.analysis2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import piloc.common.FingerprintScore;

public class State {

	public List<FingerprintScore> fingerprints;
	public List<Step> transitionSteps;
	
	public State(List<FingerprintScore> fingerprints) {
		super();
		this.fingerprints = fingerprints;
		this.transitionSteps = new ArrayList<Step>();
	}

	public void addFingerprintScore(FingerprintScore fp) {
		fingerprints.add(fp);
	}
	
	List<FingerprintScore> getTopXFingerprints(int x) {
		List<FingerprintScore> returnList = new ArrayList<FingerprintScore>();
		
		if (x > fingerprints.size()) {
			x = fingerprints.size();
		}
		
		Collections.sort(fingerprints);
		int counter = fingerprints.size() - 1;
		// We return a list of the top X likely positions, in decreasing order of score
		// Higher score = more likely
		for (int i = 0; i < x; i++) {
			returnList.add(fingerprints.get(counter));
			counter--;
		}
		
		return returnList;		
	}
	
	// Special method put in to just use for getting nicer graphics
	// Because when two fingerprints are very close together, we cannot see the effect.
	List<FingerprintScore> getTopXSeparatedFingerprints(int x) {
		List<FingerprintScore> returnList = new ArrayList<FingerprintScore>();
		
		if (x > fingerprints.size()) {
			x = fingerprints.size();
		}
		
		Collections.sort(fingerprints);
		// We return a list of the top X likely positions, in decreasing order of score
		// Higher score = more likely
		for (int i = (fingerprints.size() - 1); i >= 0; i--){
			FingerprintScore fp = fingerprints.get(i);
			boolean alreadyHaveNearby = false;
			for (FingerprintScore fp2 : returnList) {
				if (fp.getFingerprint().getPoint()
						.distance(fp2.getFingerprint().getPoint()) < 0.5 * Params.PIXELS_PER_METER) {
					alreadyHaveNearby = true;
				}
			}
			if (!alreadyHaveNearby) {
				returnList.add(fp);
			}
			if (returnList.size() >= x) {
				break;
			}
		}
		
		return returnList;		
	}
	
	public void addStep(Step step){
		transitionSteps.add(step);
	}
	
	public List<Step> getSteps() {
		return transitionSteps;
	}
	
}
