package piloc.analysis3;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import javax.imageio.ImageIO;

import piloc.common.ArrayStats;
import piloc.common.Fingerprint;
import piloc.common.FingerprintScore;
import piloc.common.PositionIdMap;
import piloc.common.RadioMap;
import piloc.common.utility.NearestMatchingPixelFinder;
import piloc.common.utility.PixelMatchCriteria;
import piloc.common.utility.ShortestPathFinder;
import piloc.common.utility.ShortestPathFinder.SPath;

public class Processor {
	
	RadioMap radioMap;
	
	int counter = 0;
	float currentAngle = 0;
	int positionNumber = 0;
	Point lastEstimatedPosition = null;
	Point lastWifiEstimatedPosition = null;
	private BufferedWriter errorStatsFileWriter;
	
	PositionIdMap positionIdMap;
	
	private BufferedImage backgroundImg;
	private BufferedImage floorPlanImg;
	private BufferedImage maskImg;
	
	private List<State> states;
	private State currentState = null;
	
	float stepLength = Params.getInitialStepLength();
	
	PixelMatchCriteria pmc = new PixelMatchCriteria(){

		@Override
		public boolean checkPixelAtPositionMatchesCriteria(
				BufferedImage img, int x, int y) {
			return new Color(img.getRGB(x, y)).equals(Color.BLACK);
		}
		
	};
	
	public Processor() {
		radioMap = new RadioMap(Params.FILE_RADIO_MAP);
		
		try {
			errorStatsFileWriter = new BufferedWriter(new FileWriter(Params.FILE_OUTPUT_DIR+"/"+"errorstats.txt"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		positionIdMap = new PositionIdMap(Params.FILE_POSITION_MAP);
		
		try {
			backgroundImg = ImageIO.read(new File(Params.FILE_BACKGROUND_IMG));
			floorPlanImg = ImageIO.read(new File(Params.FILE_FLOOR_PLAN_IMG));
			maskImg = ImageIO.read(new File(Params.FILE_FLOOR_PLAN_MASK_IMG));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		states = new ArrayList<State>();

	}
	
	public void onWifiScanReceived(String timeStamp, Fingerprint fp){
		
		int historyLength = Params.HISTORY_LENGTH;
		if (historyLength > states.size()) historyLength = states.size();
		
		if (positionNumber == 0) return;
		
		List<FingerprintScore> matches = radioMap.getWeightedFpMatchByScore(fp);
		renderWifiImage(timeStamp, matches, "wifi");
		
		NearestMatchingPixelFinder nmpf = new NearestMatchingPixelFinder(floorPlanImg, pmc);		
		
		currentState = new State(matches);
		states.add(currentState);
		
		List<FingerprintScore> currentTopXLikelyFP = currentState.getTopXFingerprints(Params.WIFI_TOP);
		List<PointScore> locationScores = new ArrayList<PointScore>();
		for (FingerprintScore fps : currentTopXLikelyFP) {
			locationScores.add(new PointScore(fps.getFingerprint().getPoint(), fps.getScore()));
		}
		
		for (int i = states.size() - historyLength; i < states.size()-1; i++) {
			
			State state = states.get(i);
			List<FingerprintScore> oldTopXLikelyFP = state.getTopXFingerprints(Params.WIFI_TOP);
				
			for (FingerprintScore fps : oldTopXLikelyFP) {
				Point oldLocation = fps.getFingerprint().getPoint();
				double oldScore = fps.getScore();
				
				for (PointScore ps : locationScores){
					Point newLocation = ps.getPoint();
					
					SPath shortestPath = new ShortestPathFinder(floorPlanImg, pmc).getShortestPath(
							nmpf.getNearestValidPixel(newLocation), 
							nmpf.getNearestValidPixel(oldLocation)
					);
					double pathLength = shortestPath.getPathLength();
					
					int currentStateNum = states.size()-1;
					int numSteps = calculateStepsBetweenStates(i, currentStateNum);
					
					// Check if path length is not so long, don't award points otherwise
					if (pathLength / numSteps < 1.2 * Params.PIXELS_PER_METER) {
						/*
						// Check if along path, angle is acceptable, don't award points otherwise
						List<Float> anglesFromSteps = getAnglesFromStepsBetweenStates(i, currentStateNum);
						List<Float> anglesFromEstimatedPath = new LinkedList<Float>();
						for (Point p : shortestPath.getPath()){
							float b = shortestPath.getBearingAtPoint(p, (int) (Params.PIXELS_PER_METER * 0.5));
							anglesFromEstimatedPath.add(b);
						}
						
						// Note we must "normalize" both angles!
						float a = (getAverageOfList(anglesFromSteps) + 360) % 360;
						float b = (getAverageOfList(anglesFromEstimatedPath) + 360) % 360;
						System.out.println(a+" "+b+" "+oldLocation+" "+newLocation);
						if (Math.abs(a - b) < 30) {*/
							double ageFactor = 1.0/(currentStateNum-i);
							//double ageFactor = historyLength - (currentStateNum-i);
							ps.setScore((float) (ps.getScore() + oldScore * ageFactor));
						/*}*/
						
						
					}
				}				
				
			}
		}
		
		renderPointScoresImage(timeStamp, locationScores, "ps");
		
		double maxScore = 0;
		Point mostLikelyPoint = null;
		for (PointScore ps : locationScores) {
			if (ps.getScore() > maxScore) {
				maxScore = ps.getScore();
				mostLikelyPoint = ps.getPoint();
			}
		}
		
		lastEstimatedPosition = mostLikelyPoint;
		
		System.out.println(counter);
		counter++;
					
		if (lastWifiEstimatedPosition == null) {
			Fingerprint mostLikelyFingerprint = matches.get(matches.size()-1).getFingerprint();
			lastWifiEstimatedPosition = new Point(mostLikelyFingerprint.getX(), mostLikelyFingerprint.getY());
			recordPositionIfDataAvailable();
		}
		
	}
	
	private float getAverageOfList(List<Float> list) {
		float sum = 0;
		
		for (Float i : list) {
			sum += i;
		}
		
		return sum / list.size();
	}

	private int calculateStepsBetweenStates(int fromState, int toState) {
		int sumSteps = 0;
		// remember that the most recent state (toState) has no steps
		// associated with it yet
		// each "state" stores the wifi fingerprint and the steps after it
		for (int i = fromState; i < toState; i++) {
			State state = states.get(i);
			sumSteps += state.getSteps().size();
		}
		return sumSteps;
	}
	
	private List<Float> getAnglesFromStepsBetweenStates(int fromState, int toState) {
		List<Float> angles = new LinkedList<Float>();
		for (int i = fromState; i < toState; i++) {
			State state = states.get(i);
			for (Step s : state.getSteps()) {
				angles.add(s.angle);
			}
		}
		return angles;
	}

	public void onStepOccurred(String timeStamp) {
		
		if (currentState == null) {
			return;
		}
		
		float stepAngle = (currentAngle + 360 + Params.MAP_ANGLE_OFFSET) % 360;
		
		currentState.addStep(new Step(stepLength, stepAngle));
		
		System.out.println(counter);
		counter++;
		
	}
	
	public void onCompassDataReceived(String timeStamp, float mCurrentAngle) {
		currentAngle = mCurrentAngle;
	}

	public void onPositionRecordOccurred(String timeStamp, int positionNumber) {
		
		// TODO Auto-generated method stub
		this.positionNumber = positionNumber;
		resetRecordPositionData();
	}
	
	public void resetRecordPositionData(){
		//lastEstimatedPosition = null;
		lastWifiEstimatedPosition = null;
	}
	
	public void recordPositionIfDataAvailable(){
		if (positionIdMap.getLocationOfId(positionNumber) == null || this.lastWifiEstimatedPosition == null) {
			return;
		}
		
		if (this.lastEstimatedPosition == null) {
			this.lastEstimatedPosition = new Point(0,0);
		}
		
		Point actualPosCoords = positionIdMap.getLocationOfId(positionNumber);
		Point estimatedPosCoords = lastEstimatedPosition;
		double estDistanceError = actualPosCoords.distance(estimatedPosCoords);
		Point wifiPosCoords = lastWifiEstimatedPosition;
		double wifiDistanceError = actualPosCoords.distance(wifiPosCoords);
		
		try {
			errorStatsFileWriter.write(String.format("%d\t%d,%d\t%d,%d\t%f\t%d,%d\t%f", 
					positionNumber,	
					actualPosCoords.x, actualPosCoords.y,
					wifiPosCoords.x, wifiPosCoords.y,
					wifiDistanceError,
					estimatedPosCoords.x, estimatedPosCoords.y,						
					estDistanceError));
			errorStatsFileWriter.newLine();
			errorStatsFileWriter.flush();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	static BufferedImage copyImage(BufferedImage bi) {
		ColorModel cm = bi.getColorModel();
		boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
		WritableRaster raster = bi.copyData(null);
		return new BufferedImage(cm, raster, isAlphaPremultiplied, null);
	}

	public void finished() {
		// TODO Auto-generated method stub
		
	}	

	private void renderPointScoresImage(String timeStamp,
			List<PointScore> locationScores, String label) {
		if (Params.RENDER_IMAGES) {

			try {
				BufferedImage img = copyImage(backgroundImg);
				Graphics2D graphic = img.createGraphics();
				
				graphic.setFont(new Font("Tahoma", Font.PLAIN, 11)); 
				
				for (PointScore ps : locationScores) {
					graphic.setColor(new Color(0.0f, 0.0f, 1.0f));
					graphic.drawString(String.format("%f", ps.getScore()), ps.getX()-5, ps.getY()+5);
					//graphic.fillOval(match.getFingerprint().getX()-5, match.getFingerprint().getY()-5, 10, 10);
				}
				
				graphic.setColor(Color.black);
				graphic.drawString(String.format("%d %s Pos:%d Wifi", counter, timeStamp, positionNumber), 10, 10);
	
				ImageIO.write(img, "PNG", new File(Params.FILE_OUTPUT_DIR+"/"+counter+" "+label+".png"));
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	private void renderWifiImage(String timeStamp, List<FingerprintScore> matches, String label) {
		if (Params.RENDER_IMAGES) {
			double maxScore = 0;
			for (FingerprintScore match : matches) {
				if (match.getScore() > maxScore) {
					maxScore = match.getScore();
				}
			}
			try {
				BufferedImage img = copyImage(backgroundImg);
				Graphics2D graphic = img.createGraphics();
				
				graphic.setFont(new Font("Tahoma", Font.PLAIN, 11)); 
				
				int fpTotalCount = matches.size();
				int fpCount = 0;
				for (FingerprintScore match : matches) {
					if (fpCount > fpTotalCount - 10) {
						graphic.setColor(new Color(1.0f, 0.0f, 0.0f, (float) (match.getScore()/maxScore)));					
					}else{
						graphic.setColor(new Color(0.0f, 0.0f, 1.0f, (float) (match.getScore()/maxScore)));
					}
					fpCount++;
					graphic.drawString(String.format("%d", (int)(match.getScore()*10000)), match.getFingerprint().getX()-5, match.getFingerprint().getY()+5);
					//graphic.fillOval(match.getFingerprint().getX()-5, match.getFingerprint().getY()-5, 10, 10);
				}
				
				graphic.setColor(Color.black);
				graphic.drawString(String.format("%d %s Pos:%d Wifi", counter, timeStamp, positionNumber), 10, 10);
	
				ImageIO.write(img, "PNG", new File(Params.FILE_OUTPUT_DIR+"/"+counter+" "+label+".png"));
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

}
