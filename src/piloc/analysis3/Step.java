package piloc.analysis3;

public class Step {

	float distance;
	float angle;
	
	public Step(float distance, float angle) {
		super();
		this.distance = distance;
		this.angle = angle;
	}
	
	public float getDistance() {
		return distance;
	}
	
	public float getAngle() {
		return angle;
	}
	
	
	
}
